////////////////////////////////////////////////////////////////////////////////
// This source file is part of the ZipArchive library source distribution and
// is Copyrighted 2000 - 2007 by Artpol Software - Tadeusz Dracz
//
// This source code is licensed as closed source and its use is 
// strictly subject to the terms and conditions of the 
// The ZipArchive Library Commercial License.
// The license is distributed with the source code in the License.txt file.
//
// Web Site: http://www.artpol-software.com
////////////////////////////////////////////////////////////////////////////////

#ifndef ZIPARCHIVE_STDAFX_DOT_H
	#error Do not include this file directly. Include stdafx.h instead
#endif

#include "_features.h"

#if _MSC_VER > 1000
	//because of STL
	#pragma warning (disable : 4710) // 'function' : function not inlined
	#pragma warning (disable : 4514) // unreferenced inline/local function has been removed
	#pragma warning (disable : 4786) // 'identifier' : identifier was truncated to 'number' characters in the debug information
#endif

#if defined (_UNICODE) && !defined (UNICODE)
	#define UNICODE
#endif
#if defined (UNICODE) && !defined (_UNICODE)
	#define _UNICODE
#endif

#ifndef _WIN32
	#ifdef _ZIP64
		#ifndef _LARGEFILE_SOURCE
			#define _LARGEFILE_SOURCE
		#endif
		#ifndef _LARGE_FILES
			#define _LARGE_FILES
		#endif
		#ifndef _FILE_OFFSET_BITS
			#define _FILE_OFFSET_BITS 64
		#endif
	#endif
	#ifndef NULL
		#define NULL    0
	#endif

	#include <ctype.h>
	typedef int HFILE;
	typedef void*				HANDLE;
	typedef unsigned long       DWORD;
	typedef long				LONG;
	typedef int                 BOOL;
	typedef unsigned char       BYTE;
	typedef unsigned short      WORD;
	typedef unsigned int        UINT;

	#ifndef FALSE
		#define FALSE               (int)0
	#endif

	#ifndef TRUE
		#define TRUE                (int)1
	#endif


	typedef unsigned short WCHAR;    // wc,   16-bit UNICODE character
	typedef const WCHAR *LPCWSTR;
	typedef const char *LPCSTR;
	typedef WCHAR *LPWSTR;
	typedef char *LPSTR;

	#ifdef  _UNICODE
		typedef wchar_t TCHAR;
		typedef LPCWSTR LPCTSTR;
		typedef LPWSTR LPTSTR;
		#define _T(x)      L ## x
	#else   /* _UNICODE */               // r_winnt
		typedef char TCHAR;
		typedef LPCSTR LPCTSTR;
		typedef LPSTR LPTSTR;
		#define _T(x)      x
	#endif /* _UNICODE */                // r_winnt

	typedef unsigned long long ULONGLONG;
	typedef long long LONGLONG;
	#define CP_ACP 0
	#define CP_OEMCP 1
	
	#ifndef _I64_MAX
		#ifdef LLONG_MAX	
			#define _I64_MAX LLONG_MAX
		#else
			#define _I64_MAX LONG_LONG_MAX
		#endif
	#endif
	#ifndef _UI64_MAX
		#ifdef ULLONG_MAX	
			#define _UI64_MAX ULLONG_MAX
		#else
			#define _UI64_MAX ULONG_LONG_MAX
		#endif
	#endif
	#define _lseeki64 lseek64
#else
	#include <TCHAR.H>
   	#include <windows.h>
	#include <stddef.h>
  	#ifndef STRICT
		#define STRICT
	#endif	
	
#endif	// #ifndef _WIN32

#ifndef ASSERT
	#include <assert.h>
	#define ASSERT(f) assert((f))
#endif
#ifndef VERIFY
	#ifdef _DEBUG
		#define VERIFY(x) ASSERT((x))
	#else
		#define VERIFY(x) x
	#endif
#endif

#if !defined(_INTPTR_T_DEFINED) && !defined(__GNUC__)
	typedef long intptr_t;
#endif

#define ZIP_FILE_USIZE ULONGLONG
#define ZIP_FILE_SIZE LONGLONG
#define ZIP_FILE_SIZEMAX _I64_MAX
