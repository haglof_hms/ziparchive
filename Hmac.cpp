////////////////////////////////////////////////////////////////////////////////
// This source file is part of the ZipArchive library source distribution and
// is Copyrighted 2000 - 2007 by Artpol Software - Tadeusz Dracz
//
// This source code is licensed as closed source and its use is 
// strictly subject to the terms and conditions of the 
// The ZipArchive Library Commercial License.
// The license is distributed with the source code in the License.txt file.
//
// Web Site: http://www.artpol-software.com
////////////////////////////////////////////////////////////////////////////////

#include "_features.h"

#ifdef _ZIP_AES

/*
  
  ADDITIONAL INFORMATION

  The source code in this file is based on Dr Brian Gladman implementation of the AES algorithm,
  which at the moment of writing is freely available at http://fp.gladman.plus.com/AES/index.htm.
  The AES source code is reused under the permission granted by Dr Brian Gladman in original sources
  in a copyright notice which is reproduced below:
  

> ---------------------------------------------------------------------------
> Copyright (c) 2003, Dr Brian Gladman, Worcester, UK.   All rights reserved.
>
> LICENSE TERMS
>
> The free distribution and use of this software in both source and binary
> form is allowed (with or without changes) provided that:
>
>   1. distributions of this source code include the above copyright
>      notice, this list of conditions and the following disclaimer;
>
>   2. distributions in binary form include the above copyright
>      notice, this list of conditions and the following disclaimer
>      in the documentation and/or other associated materials;
>
>   3. the copyright holder's name is not used to endorse products
>      built using this software without specific written permission.
>
> ALTERNATIVELY, provided that this notice is retained in full, this product
> may be distributed under the terms of the GNU General Public License (GPL),
> in which case the provisions of the GPL apply INSTEAD OF those given above.
>
> DISCLAIMER
>
> This software is provided 'as is' with no explicit or implied warranties
> in respect of its properties, including, but not limited to, correctness
> and/or fitness for purpose.
>
> ---------------------------------------------------------------------------
*/

#include "stdafx.h"
#include "Hmac.h"
#include "limits.h"

CHmac::CHmac()
{
	memset(m_key, 0, ZIP_SHA1_BLOCK_SIZE);
	m_uKeyLength = 0;
}

bool CHmac::SHA1Key(const char* pKey, UINT uKeyLength)
{
	if(m_uKeyLength == UINT_MAX)        /* error if further key input   */
        return false;						/* is attempted in data mode    */

    if(m_uKeyLength + uKeyLength > ZIP_SHA1_BLOCK_SIZE)    /* if the key has to be hashed  */
    {
        if(m_uKeyLength <= ZIP_SHA1_BLOCK_SIZE)         /* if the hash has not yet been */
        {                                       /* started, initialise it and   */
            m_sha1.Begin();                /* hash stored key characters   */
			m_sha1.Hash(m_key, m_uKeyLength);
        }

        m_sha1.Hash(pKey, uKeyLength);       /* hash long key data into hash */
    }
    else                                        /* otherwise store key data     */
		memcpy(m_key + m_uKeyLength, pKey, uKeyLength);

	m_uKeyLength += uKeyLength; /* update the key length count  */
	return true;
}

void CHmac::Data(const char* pData, UINT uSize)
{
	if(m_uKeyLength != UINT_MAX)                /* if not yet in data phase */
    {
        if(m_uKeyLength > ZIP_SHA1_BLOCK_SIZE)          /* if key is being hashed   */
        {                                       /* complete the hash and    */
            m_sha1.End(m_key);         /* store the result as the  */
            m_uKeyLength = ZIP_SHA1_DIGEST_SIZE;        /* key and set new length   */
        }

        /* pad the key if necessary */
        memset(m_key + m_uKeyLength, 0, ZIP_SHA1_BLOCK_SIZE - m_uKeyLength);

        /* xor ipad into key value  */
        for(UINT i = 0; i < (ZIP_SHA1_BLOCK_SIZE >> 2); ++i)
            ((unsigned long*)m_key)[i] ^= 0x36363636;

        /* and start hash operation */
		m_sha1.Begin();
		m_sha1.Hash((char*)m_key, ZIP_SHA1_BLOCK_SIZE);

        /* mark as now in data mode */
		m_uKeyLength = UINT_MAX;
    }

    /* hash the data (if any)       */
    if(uSize)
		m_sha1.Hash(pData, uSize);
}

void CHmac::End(char* pData, UINT uSize)
{
	char dig[ZIP_SHA1_DIGEST_SIZE];

    /* if no data has been entered perform a null data phase        */
    if(m_uKeyLength != UINT_MAX)
        Data(NULL, 0);
	m_sha1.End(dig); /* complete the inner hash      */

	UINT i;
    /* set outer key value using opad and removing ipad */
    for(i = 0; i < (ZIP_SHA1_BLOCK_SIZE >> 2); ++i)
        ((unsigned long*)m_key)[i] ^= 0x36363636 ^ 0x5c5c5c5c;

    /* perform the outer hash operation */
	m_sha1.Begin();
	m_sha1.Hash(m_key, ZIP_SHA1_BLOCK_SIZE);
	m_sha1.Hash(dig, ZIP_SHA1_DIGEST_SIZE);
	m_sha1.End(dig);

    /* output the hash value            */
    for(i = 0; i < uSize; ++i)
        pData[i] = dig[i];
}

CHmac::~CHmac()
{
}

#endif
