////////////////////////////////////////////////////////////////////////////////
// This source file is part of the ZipArchive library source distribution and
// is Copyrighted 2000 - 2007 by Artpol Software - Tadeusz Dracz
//
// This source code is licensed as closed source and its use is 
// strictly subject to the terms and conditions of the 
// The ZipArchive Library Commercial License.
// The license is distributed with the source code in the License.txt file.
//
// Web Site: http://www.artpol-software.com
////////////////////////////////////////////////////////////////////////////////

#ifdef _ZIP_AES

/*
  
  ADDITIONAL INFORMATION

  The source code in this file is based on Dr Brian Gladman implementation of the AES algorithm,
  which at the moment of writing is freely available at http://fp.gladman.plus.com/AES/index.htm.
  The AES source code is reused under the permission granted by Dr Brian Gladman in original sources
  in a copyright notice which is reproduced below:
  

> ---------------------------------------------------------------------------
> Copyright (c) 2003, Dr Brian Gladman, Worcester, UK.   All rights reserved.
>
> LICENSE TERMS
>
> The free distribution and use of this software in both source and binary
> form is allowed (with or without changes) provided that:
>
>   1. distributions of this source code include the above copyright
>      notice, this list of conditions and the following disclaimer;
>
>   2. distributions in binary form include the above copyright
>      notice, this list of conditions and the following disclaimer
>      in the documentation and/or other associated materials;
>
>   3. the copyright holder's name is not used to endorse products
>      built using this software without specific written permission.
>
> ALTERNATIVELY, provided that this notice is retained in full, this product
> may be distributed under the terms of the GNU General Public License (GPL),
> in which case the provisions of the GPL apply INSTEAD OF those given above.
>
> DISCLAIMER
>
> This software is provided 'as is' with no explicit or implied warranties
> in respect of its properties, including, but not limited to, correctness
> and/or fitness for purpose.
>
> ---------------------------------------------------------------------------
*/

/**
* \file ZipAesCryptograph.h
*	Interface for the CZipAesCryptograph class.
*
*/

#if !defined(ZIPARCHIVE_ZIPAESCRYPTOGRAPH_DOT_H)
#define ZIPARCHIVE_ZIPAESCRYPTOGRAPH_DOT_H

#if _MSC_VER > 1000
	#pragma once
#endif // _MSC_VER > 1000

#include "ZipCryptograph.h"
#include "ZipFileHeader.h"
#include "ZipStorage.h"
#include "ZipAutoBuffer.h"
#include "RandomPool.h"
#include "ZipException.h"
#include "Aes.h"
#include "Hmac.h"


#define ZIP_AES_MAC_SIZE 10


/**
	Performs AES encryption.
	Uses a key obtained using key derivation from a password and a salt value method
	as described in RFC2898.

	\see
		<a href="kb">0610201627|aes</a>
 */
class ZIP_API CZipAesCryptograph : public CZipCryptograph
{
	// TODO: make it multithreading safe
	static CRandomPool m_pool;


	/**
		Initializes a new instance of the CZipAesCryptograph class.
		
		\param iStrength
			The algorithm's strength. One of the CZipCryptograph::EncryptionMethod values, 
			that is related to AES encryption.
		
		\throws CZipException
			with the CZipException::genericError code, if \a iStrength is invalid.		
	 */
public:
	CZipAesCryptograph(int iStrength);

#ifdef _ZIP_DOC_GENERATION
	bool InitDecode(CZipAutoBuffer& password, CZipFileHeader& currentFile, CZipStorage& storage);
	void FinishDecode(CZipFileHeader& currentFile, CZipStorage& storage);
#else
	bool InitDecode(CZipAutoBuffer& password, CZipFileHeader&, CZipStorage& storage);
	void FinishDecode(CZipFileHeader&, CZipStorage& storage);
#endif
	void InitEncode(CZipAutoBuffer& password, CZipFileHeader& currentFile, CZipStorage& storage);
	void FinishEncode(CZipFileHeader& currentFile, CZipStorage& storage);
	
	
	void Decode(char* pBuffer, DWORD uSize)
	{		
		if (!m_pAes)
		{
			ASSERT(FALSE);
			return;
		}		
		m_hmac.Data(pBuffer, uSize);
		m_pAes->Encrypt(pBuffer, uSize);
	}

	void Encode(char* pBuffer, DWORD uSize)
	{
		if (!m_pAes)
		{
			ASSERT(FALSE);
			return;
		}
		m_pAes->Encrypt(pBuffer, uSize);
		m_hmac.Data(pBuffer, uSize);
	}

	static DWORD GetEncryptedInfoSize(int iStrength);
	static int GetKeySize(int iStrength);

	/**
		Implements RFC2898, which specifies key derivation from a password and a salt value.
		
		\param[in] password
			The password.
		
		\param[in] salt
			The salt value.
		
		\param iIterations
			The number of iterations to use.
		
		\param[out] key
			The derived key.	
	 */
	static void DeriveKey(const CZipAutoBuffer& password, const CZipAutoBuffer& salt, UINT iIterations, CZipAutoBuffer& key);
private:
	static int GetSaltSize(int iStrength);
	void Init(const CZipAutoBuffer& password, const CZipAutoBuffer& salt, CZipAutoBuffer& verifier);
	void Finish(CZipAutoBuffer& mac)
	{
		mac.Allocate(ZIP_AES_MAC_SIZE, true);
		m_hmac.End(mac, ZIP_AES_MAC_SIZE);
	}
	void ClearAes()
	{
		if (m_pAes != NULL)
		{
			delete m_pAes;
			m_pAes = NULL;
		}
	}
	CAes* m_pAes;
	int m_iStrength;
	CHmac m_hmac;
public:
	~CZipAesCryptograph()
	{
		ClearAes();
	}
};


#endif

#endif
