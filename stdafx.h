////////////////////////////////////////////////////////////////////////////////
// This source file is part of the ZipArchive library source distribution and
// is Copyrighted 2000 - 2007 by Artpol Software - Tadeusz Dracz
//
// This source code is licensed as closed source and its use is 
// strictly subject to the terms and conditions of the 
// The ZipArchive Library Commercial License.
// The license is distributed with the source code in the License.txt file.
//
// Web Site: http://www.artpol-software.com
////////////////////////////////////////////////////////////////////////////////

#if !defined(ZIPARCHIVE_STDAFX_DOT_H)
#define ZIPARCHIVE_STDAFX_DOT_H

#include "_features.h"
#include "_platform.h"

#if _MSC_VER > 1000
	#pragma once
#endif // _MSC_VER > 1000

// uncomment to disable compiling standard error messages into the library
#define ZIP_ENABLE_ERROR_DESCRIPTION

 

#if _MSC_VER < 1300 && !defined __BORLANDC__ && !defined (__GNUC__)
	#define ZIPINLINE inline
#else
	#define ZIPINLINE
#endif

#ifdef ZIP_ARCHIVE_STL
	#include "std_stl.h"
#else
	#include "std_mfc.h"
#endif

#ifdef TRACE
	#if _MSC_VER >= 1300
		#define ZIPTRACE(f) TRACE(f,__FILE__,__LINE__)
	#else
		#define ZIPTRACE(f) TRACE(_T(f))
	#endif
#else
	#define ZIPTRACE(f)
	#define NOZIPTRACE
#endif

#ifdef _ZIP64
	#define ZIP_INDEX_TYPE ULONGLONG
	#define ZIP_PART_TYPE DWORD
	#define ZIP_SIZE_TYPE ULONGLONG
	#define ZIP_ZLIB_TYPE LONGLONG
#else
	#ifdef _ZIP_STRICT_U16
		#define ZIP_INDEX_TYPE WORD
		#define ZIP_PART_TYPE WORD		
	#else
		#define ZIP_INDEX_TYPE int
		#define ZIP_PART_TYPE int
	#endif	

	#define ZIP_SIZE_TYPE DWORD
	#define ZIP_ZLIB_TYPE int
#endif

#define ZIP_FILE_INDEX_NOT_FOUND ZIP_INDEX_TYPE(-1)
#define ZIP_FILE_INDEX_UNSPECIFIED ZIP_FILE_INDEX_NOT_FOUND
#define ZIP_DISK_NUMBER_UNSPECIFIED ZIP_PART_TYPE(-1)

#endif // !defined(ZIPARCHIVE_STDAFX_DOT_H)
 
