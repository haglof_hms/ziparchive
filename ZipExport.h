////////////////////////////////////////////////////////////////////////////////
// This source file is part of the ZipArchive library source distribution and
// is Copyrighted 2000 - 2007 by Artpol Software - Tadeusz Dracz
//
// This source code is licensed as closed source and its use is 
// strictly subject to the terms and conditions of the 
// The ZipArchive Library Commercial License.
// The license is distributed with the source code in the License.txt file.
//
// Web Site: http://www.artpol-software.com
////////////////////////////////////////////////////////////////////////////////

#if !defined (ZIPARCHIVE_ZIPEXPORT_DOT_H)
#define ZIPARCHIVE_ZIPEXPORT_DOT_H

#if defined (ZIP_HAS_DLL)
#    if defined (ZIP_BUILD_DLL)
#      define ZIP_API __declspec (dllexport)
#    else
#      define ZIP_API  __declspec (dllimport)
#	endif
#else
#  define ZIP_API
#endif     /* ZIP_HAS_DLL */

#endif     /* ZIPARCHIVE_ZIPEXPORT_DOT_H */
