////////////////////////////////////////////////////////////////////////////////
// This source file is part of the ZipArchive library source distribution and
// is Copyrighted 2000 - 2007 by Artpol Software - Tadeusz Dracz
//
// This source code is licensed as closed source and its use is 
// strictly subject to the terms and conditions of the 
// The ZipArchive Library Commercial License.
// The license is distributed with the source code in the License.txt file.
//
// Web Site: http://www.artpol-software.com
////////////////////////////////////////////////////////////////////////////////

/**
* \file BytesWriter.h
*	Interface for the CBytesWriter class.
*
*/

#if !defined(ZIPARCHIVE_BYTESWRITER_DOT_H)
#define ZIPARCHIVE_BYTESWRITER_DOT_H

#if _MSC_VER > 1000
	#pragma once
#endif // _MSC_VER > 1000

#include "ZipCompatibility.h"


/**
	Provides implementation for various 
	buffer operations depending on the current platform and configuration.
 */
class ZIP_API CBytesWriter
{
public:
	
#ifdef ZIP_ARCHIVE_LITTLE_ENDIAN
	/**
		Reads \a iCount bytes from \a pSource into \a pDestination.

		\param[out] pDestination
			The buffer to retrieve data with byte-ordering depending on the machine.

		\param[in] pSource
			The buffer with little-endian ordered data.

		\param iCount 
			The number of bytes to read.
	*/
	static void ReadBytes(void* pDestination, const char* pSource, int iCount)
	{
		memcpy(pDestination, pSource, iCount);
	}

	/**
		Writes \a iCount bytes from \a pSource into \a pDestination.

		\param[out] pDestination
			The buffer to retrieve little-endian ordered data.

		\param[in] pSource
			The buffer with byte-ordering depending on the machine.

		\param iCount 
			The number of bytes to write.
	*/
	static void WriteBytes(char* pDestination, const void* pSource, int iCount)
	{
		memcpy(pDestination, pSource, iCount);
	}

	/**
		Compares \a iCount bytes.

		\param[in] pBuffer
			The buffer with little-endian ordered data.

		\param[in] pBytes
			The buffer with byte-ordering depending on the machine.

		\param iCount 
			The number of bytes to compare.
	*/
	static bool CompareBytes(const char* pBuffer, const void* pBytes, int iCount)
	{
		return memcmp(pBuffer, pBytes, iCount) == 0;
	}
#else
	static void ReadBytes(void* pDestination, const char* pSource, int iCount)
	{
		for (int i = 0; i < iCount; i++)
			((char*)pDestination)[i] = pSource[iCount - i - 1];
	}

	static void WriteBytes(char* pDestination, const void* pSource, int iCount)
	{
		for (int i = 0; i < iCount; i++)
			pDestination[i] = ((char*)pSource)[iCount - i - 1];
	}

	static bool CompareBytes(const char* pBuffer, const void* pBytes, int iCount)
	{
		for (int i = 0; i < iCount; i++)
			if (pBuffer[i] != ((char*)pBytes)[iCount - i - 1])
				return false;
		return true;
	}

#endif
	
#ifdef _ZIP64	
	/**
		Writes safely a 64-bit value to a 32-bit value. If the 64-bit value is larger than 
		\c UINT_MAX, then \c UINT_MAX is written; otherwise the original value is written.
		
		\param uValue
			The value to write.
		
		\returns
			A safe value.
	 */
	static const DWORD& WriteSafeU32(const ZIP_SIZE_TYPE& uValue)
	{
		return uValue > UINT_MAX ? m_guI32Max : (DWORD&)uValue;
	}

	/**
		Writes safely a 32-bit value to a 16-bit value. If the 32-bit value is larger than 
		\c USHRT_MAX, then \c USHRT_MAX is written; otherwise the original value is written.
		
		\param uValue
			The value to write.
		
		\returns
			A safe value.
	 */
	static const WORD& WriteSafeU16(const DWORD& uValue)
	{
		return uValue > USHRT_MAX ? m_guI16Max : (WORD&)uValue;
	}

	/**
		Writes safely a 64-bit value to a 16-bit value. If the 64-bit value is larger than 
		\c USHRT_MAX, then \c USHRT_MAX is written; otherwise the original value is written.
		
		\param uValue
			The value to write.
		
		\returns
			A safe value.
	 */
	static const WORD& WriteSafeU16(const ULONGLONG& uValue)
	{
		return uValue > USHRT_MAX ? m_guI16Max : (WORD&)uValue;
	}
#else
	static const DWORD& WriteSafeU32(const DWORD& uValue)
	{
		return uValue;
	}

#ifdef _ZIP_STRICT_U16
	static const WORD& WriteSafeU16(const WORD& uValue)
	{
		return uValue;
	}
#else
	static const WORD& WriteSafeU16(const int& uValue)
	{
		return (WORD&)uValue;
	}
#endif

#endif

#ifdef _ZIP64
protected:
	static DWORD m_guI32Max;
	static WORD m_guI16Max;
#endif

};

#endif
