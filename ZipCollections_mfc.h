////////////////////////////////////////////////////////////////////////////////
// This source file is part of the ZipArchive library source distribution and
// is Copyrighted 2000 - 2007 by Artpol Software - Tadeusz Dracz
//
// This source code is licensed as closed source and its use is 
// strictly subject to the terms and conditions of the 
// The ZipArchive Library Commercial License.
// The license is distributed with the source code in the License.txt file.
//
// Web Site: http://www.artpol-software.com
////////////////////////////////////////////////////////////////////////////////

#ifndef ZIPARCHIVE_ZIPCOLLECTIONS_DOT_H
	#error Do not include this file directly. Include ZipCollections.h instead
#endif

#if _MSC_VER > 1000
	#pragma warning( push )
	#pragma warning (disable:4786) // 'identifier' : identifier was truncated to 'number' characters in the debug information
#endif // _MSC_VER > 1000

#include <afxtempl.h>

#define ZIP_ARRAY_SIZE_TYPE INT_PTR

typedef CStringArray CZipStringArray;

template <class TYPE>
class CZipArray : public CArray<TYPE, TYPE>
{
	static int CompareAsc(const void *pArg1, const void *pArg2)
	{
		TYPE w1 = *(TYPE*)pArg1;
		TYPE w2 = *(TYPE*)pArg2;
		return w1 == w2 ? 0 :(w2 > w1 ? - 1 : 1);
	}
	static int CompareDesc(const void *pArg1, const void *pArg2)
	{
		TYPE w1 = *(TYPE*)pArg1;
		TYPE w2 = *(TYPE*)pArg2;
		return w1 == w2 ? 0 :(w1 > w2 ? - 1 : 1);		
	}
public:	
	void Sort(bool bAscending)
	{
		ZIP_INDEX_TYPE uSize = (ZIP_INDEX_TYPE)GetSize();
		if (!uSize) // if ommitted operator [] will fail if empty
			return;
		qsort((void*)&((*this)[0]), (size_t)uSize , sizeof(TYPE), bAscending ? CompareAsc : CompareDesc);
	}
};

template<class TYPE>
class CZipPtrList : public CTypedPtrList<CPtrList, TYPE>
{
public:
	typedef POSITION iterator;
	typedef POSITION const_iterator;

	bool IteratorValid(const iterator &iter) const
	{
		return iter != NULL;
	}

};

template<class KEY, class VALUE>
class CZipMap : public CMap<KEY, KEY, VALUE, VALUE>
{
	
};

#if _MSC_VER > 1000
	#pragma warning( pop )
#endif