////////////////////////////////////////////////////////////////////////////////
// This source file is part of the ZipArchive library source distribution and
// is Copyrighted 2000 - 2007 by Artpol Software - Tadeusz Dracz
//
// This source code is licensed as closed source and its use is 
// strictly subject to the terms and conditions of the 
// The ZipArchive Library Commercial License.
// The license is distributed with the source code in the License.txt file.
//
// Web Site: http://www.artpol-software.com
////////////////////////////////////////////////////////////////////////////////

#include "_features.h"

#ifdef _ZIP_AES

/*
  
  ADDITIONAL INFORMATION

  The source code in this file is based on Dr Brian Gladman implementation of the AES algorithm,
  which at the moment of writing is freely available at http://fp.gladman.plus.com/AES/index.htm.
  The AES source code is reused under the permission granted by Dr Brian Gladman in original sources
  in a copyright notice which is reproduced below:
  

> ---------------------------------------------------------------------------
> Copyright (c) 2003, Dr Brian Gladman, Worcester, UK.   All rights reserved.
>
> LICENSE TERMS
>
> The free distribution and use of this software in both source and binary
> form is allowed (with or without changes) provided that:
>
>   1. distributions of this source code include the above copyright
>      notice, this list of conditions and the following disclaimer;
>
>   2. distributions in binary form include the above copyright
>      notice, this list of conditions and the following disclaimer
>      in the documentation and/or other associated materials;
>
>   3. the copyright holder's name is not used to endorse products
>      built using this software without specific written permission.
>
> ALTERNATIVELY, provided that this notice is retained in full, this product
> may be distributed under the terms of the GNU General Public License (GPL),
> in which case the provisions of the GPL apply INSTEAD OF those given above.
>
> DISCLAIMER
>
> This software is provided 'as is' with no explicit or implied warranties
> in respect of its properties, including, but not limited to, correctness
> and/or fitness for purpose.
>
> ---------------------------------------------------------------------------
*/

#include "stdafx.h"
#include "RandomPool.h"
#include "ZipException.h"

CRandomPool::CRandomPool()
{
	Clear();

	Update();

	/* mix the pool a minimum number of times               */
    for(UINT i = 0; i < ZIP_PRNG_MIN_MIX; ++i)
        Mix(m_pool);

    /* update the pool to prime the pool output buffer      */
    Update();
}

void CRandomPool::Update()
{
	memcpy(m_output, m_pool, ZIP_PRNG_POOL_SIZE);

	UINT iPos = 0;
    /* enter entropy data into the pool */
#ifdef ZIP_ARCHIVE_WIN	
	LARGE_INTEGER counter;
	if (QueryPerformanceFrequency(&counter) != 0)
	{		
		while (iPos < ZIP_PRNG_POOL_SIZE)
		{
			if (QueryPerformanceCounter(&counter))
			{
				char* buf = (char*)&counter;
				int i = 0;
				while (i < 8 && iPos < ZIP_PRNG_POOL_SIZE)
					m_pool[iPos++] = buf[i++];
			}
		}
	}
#endif	
	if (iPos < ZIP_PRNG_POOL_SIZE)
	{		
#if (_MSC_VER >= 1400)
		UINT rnd;
#else
		srand((UINT)time(NULL));		
#endif
		do
		{
#if (_MSC_VER >= 1400)
			if (rand_s(&rnd))
				CZipException::Throw(CZipException::internalError);
			m_pool[iPos++] = (char)rnd;
#else
			m_pool[iPos++] = (char)rand();
#endif		
		}
		while (iPos < ZIP_PRNG_POOL_SIZE);
	}

    /* invert and xor the original pool data into the pool  */
    for(int i = 0; i < ZIP_PRNG_POOL_SIZE; ++i)
        m_pool[i] ^= ~m_output[i];

	/* mix the pool and the output buffer   */
	Mix(m_pool);
	Mix(m_output);
}

void CRandomPool::Mix(BYTE* buffer)
{ 	
    CSha1 sha1;

    /*lint -e{663}  unusual array to pointer conversion */
    for(UINT i = 0; i < ZIP_PRNG_POOL_SIZE; i += ZIP_SHA1_DIGEST_SIZE)
    {
        /* copy digest size pool block into SHA1 hash block */
        memcpy(sha1.m_hash, buffer + (i ? i : ZIP_PRNG_POOL_SIZE) - ZIP_SHA1_DIGEST_SIZE, ZIP_SHA1_DIGEST_SIZE);

        /* copy data from pool into the SHA1 data buffer    */
        UINT len = ZIP_PRNG_POOL_SIZE - i;
        memcpy(sha1.m_wbuf, buffer + i, (len > ZIP_SHA1_BLOCK_SIZE ? ZIP_SHA1_BLOCK_SIZE : len));

        if (len < ZIP_SHA1_BLOCK_SIZE)
            memcpy(((char*)sha1.m_wbuf) + len, buffer, ZIP_SHA1_BLOCK_SIZE - len);

        /* compress using the SHA1 compression function     */
		sha1.Compile();        

        /* put digest size block back into the random pool  */
        memcpy(buffer + i, sha1.m_hash, ZIP_SHA1_DIGEST_SIZE);
    }
}

void CRandomPool::GetRandom(CZipAutoBuffer& buffer)
{
	char *rp = buffer;
	UINT dataLen = buffer.GetSize();
    while(dataLen)
    {
        /* transfer 'dataLen' bytes (or the number of bytes remaining  */
        /* the pool output buffer if less) into the output              */
        UINT len = (dataLen < ZIP_PRNG_POOL_SIZE - m_uPosition ? dataLen : ZIP_PRNG_POOL_SIZE - m_uPosition);
        memcpy(rp, m_output + m_uPosition, len);
        rp += len;          /* update ouput buffer position pointer     */
        m_uPosition += len;         /* update pool output buffer pointer        */
        dataLen -= len;    /* update the remaining data count          */

        /* refresh the random pool if necessary */
        if(m_uPosition == ZIP_PRNG_POOL_SIZE)
        {
            Update();
			m_uPosition = 0;
        }
    }
}

// this would require CRT initialization when used in managed application
//CRandomPool::~CRandomPool()
//{
//	Clear();
//}

#endif
