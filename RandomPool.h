////////////////////////////////////////////////////////////////////////////////
// This source file is part of the ZipArchive library source distribution and
// is Copyrighted 2000 - 2007 by Artpol Software - Tadeusz Dracz
//
// This source code is licensed as closed source and its use is 
// strictly subject to the terms and conditions of the 
// The ZipArchive Library Commercial License.
// The license is distributed with the source code in the License.txt file.
//
// Web Site: http://www.artpol-software.com
////////////////////////////////////////////////////////////////////////////////

#ifdef _ZIP_AES

/*
  
  ADDITIONAL INFORMATION

  The source code in this file is based on Dr Brian Gladman implementation of the AES algorithm,
  which at the moment of writing is freely available at http://fp.gladman.plus.com/AES/index.htm.
  The AES source code is reused under the permission granted by Dr Brian Gladman in original sources
  in a copyright notice which is reproduced below:
  

> ---------------------------------------------------------------------------
> Copyright (c) 2003, Dr Brian Gladman, Worcester, UK.   All rights reserved.
>
> LICENSE TERMS
>
> The free distribution and use of this software in both source and binary
> form is allowed (with or without changes) provided that:
>
>   1. distributions of this source code include the above copyright
>      notice, this list of conditions and the following disclaimer;
>
>   2. distributions in binary form include the above copyright
>      notice, this list of conditions and the following disclaimer
>      in the documentation and/or other associated materials;
>
>   3. the copyright holder's name is not used to endorse products
>      built using this software without specific written permission.
>
> ALTERNATIVELY, provided that this notice is retained in full, this product
> may be distributed under the terms of the GNU General Public License (GPL),
> in which case the provisions of the GPL apply INSTEAD OF those given above.
>
> DISCLAIMER
>
> This software is provided 'as is' with no explicit or implied warranties
> in respect of its properties, including, but not limited to, correctness
> and/or fitness for purpose.
>
> ---------------------------------------------------------------------------
*/

/**
* \file RandomPool.h
*	Interface for the CRandomPool class.
*
*/

#if !defined(ZIPARCHIVE_RANDOMPOOL_DOT_H)
#define ZIPARCHIVE_RANDOMPOOL_DOT_H

#if _MSC_VER > 1000
	#pragma once
#endif // _MSC_VER > 1000

#include "ZipAutoBuffer.h"
#include "Sha1.h"

#define ZIP_PRNG_POOL_LEN    256    /* minimum random pool size             */
#define ZIP_PRNG_MIN_MIX      20    /* min initial pool mixing iterations   */

#define ZIP_PRNG_POOL_SIZE  (ZIP_SHA1_DIGEST_SIZE * (1 + (ZIP_PRNG_POOL_LEN - 1) / ZIP_SHA1_DIGEST_SIZE))


/**
	Implements a random data pool based on the use of an external
	entropy function.  It is based on the ideas advocated by Peter Gutmann in
	his work on pseudo random sequence generators.
 */
class ZIP_API CRandomPool
{
public:
	CRandomPool();	
	//~CRandomPool();	
	/**
		Provides random bytes from the random data pool.

		\param[out] buffer
			The buffer to retrieve results.
	 */
	void GetRandom(CZipAutoBuffer& buffer);
	void Clear()
	{
		memset(m_pool, 0, ZIP_PRNG_POOL_SIZE);
		memset(m_output, 0, ZIP_PRNG_POOL_SIZE);	
	}
protected:
	/**
		Refreshes the output buffer and updates the random pool by adding entropy and remixing.
	 */
	void Update();
private:
	void Mix(BYTE* buffer);	
	BYTE m_pool[ZIP_PRNG_POOL_SIZE], m_output[ZIP_PRNG_POOL_SIZE];
	UINT m_uPosition;
};


#endif

#endif
