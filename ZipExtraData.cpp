////////////////////////////////////////////////////////////////////////////////
// This source file is part of the ZipArchive library source distribution and
// is Copyrighted 2000 - 2007 by Artpol Software - Tadeusz Dracz
//
// This source code is licensed as closed source and its use is 
// strictly subject to the terms and conditions of the 
// The ZipArchive Library Commercial License.
// The license is distributed with the source code in the License.txt file.
//
// Web Site: http://www.artpol-software.com
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ZipArchive.h"
#include "ZipExtraData.h"
#include "BytesWriter.h"

bool CZipExtraData::Read(char* buffer, WORD uSize)
{
	if (uSize < 4)
			return false;
	WORD size;
	CBytesWriter::ReadBytes(&m_uHeaderID, buffer, 2);
	CBytesWriter::ReadBytes(&size, buffer + 2, 2);
	if (uSize - 4 < size)
		return false;
	m_data.Allocate(size);
	memcpy(m_data, buffer + 4, size);
	return true;
}

WORD CZipExtraData::Write(char* buffer)const
{
	CBytesWriter::WriteBytes(buffer, &m_uHeaderID, 2);
	WORD size = (WORD)m_data.GetSize();
	CBytesWriter::WriteBytes(buffer + 2, &size, 2);
	memcpy(buffer + 4, m_data, size);
	return size + 4;
}
