////////////////////////////////////////////////////////////////////////////////
// This source file is part of the ZipArchive library source distribution and
// is Copyrighted 2000 - 2007 by Artpol Software - Tadeusz Dracz
//
// This source code is licensed as closed source and its use is 
// strictly subject to the terms and conditions of the 
// The ZipArchive Library Commercial License.
// The license is distributed with the source code in the License.txt file.
//
// Web Site: http://www.artpol-software.com
////////////////////////////////////////////////////////////////////////////////

#ifndef ZIPARCHIVE_ZIPBASEEXCEPTION_DOT_H
#define ZIPARCHIVE_ZIPBASEEXCEPTION_DOT_H

	#ifdef ZIP_ARCHIVE_STL
	typedef std::exception CZipBaseException;
	#else
	typedef CException CZipBaseException;
#endif

#endif //ZIPARCHIVE_ZIPBASEEXCEPTION_DOT_H
