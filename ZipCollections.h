////////////////////////////////////////////////////////////////////////////////
// This source file is part of the ZipArchive library source distribution and
// is Copyrighted 2000 - 2007 by Artpol Software - Tadeusz Dracz
//
// This source code is licensed as closed source and its use is 
// strictly subject to the terms and conditions of the 
// The ZipArchive Library Commercial License.
// The license is distributed with the source code in the License.txt file.
//
// Web Site: http://www.artpol-software.com
////////////////////////////////////////////////////////////////////////////////

#ifndef ZIPARCHIVE_ZIPCOLLECTIONS_DOT_H
#define ZIPARCHIVE_ZIPCOLLECTIONS_DOT_H

#if _MSC_VER > 1000
	#pragma once
#endif // _MSC_VER > 1000

#include "_platform.h"
#include "ZipExport.h"

#ifdef ZIP_ARCHIVE_STL
	#include "ZipCollections_stl.h"
#else
	#include "ZipCollections_mfc.h"	
#endif

typedef CZipArray<ZIP_INDEX_TYPE> CZipIndexesArray;

#endif  /* ZIPARCHIVE_ZIPCOLLECTIONS_DOT_H */

