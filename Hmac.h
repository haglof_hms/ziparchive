////////////////////////////////////////////////////////////////////////////////
// This source file is part of the ZipArchive library source distribution and
// is Copyrighted 2000 - 2007 by Artpol Software - Tadeusz Dracz
//
// This source code is licensed as closed source and its use is 
// strictly subject to the terms and conditions of the 
// The ZipArchive Library Commercial License.
// The license is distributed with the source code in the License.txt file.
//
// Web Site: http://www.artpol-software.com
////////////////////////////////////////////////////////////////////////////////

#ifdef _ZIP_AES

/*
  
  ADDITIONAL INFORMATION

  The source code in this file is based on Dr Brian Gladman implementation of the AES algorithm,
  which at the moment of writing is freely available at http://fp.gladman.plus.com/AES/index.htm.
  The AES source code is reused under the permission granted by Dr Brian Gladman in original sources
  in a copyright notice which is reproduced below:
  

> ---------------------------------------------------------------------------
> Copyright (c) 2003, Dr Brian Gladman, Worcester, UK.   All rights reserved.
>
> LICENSE TERMS
>
> The free distribution and use of this software in both source and binary
> form is allowed (with or without changes) provided that:
>
>   1. distributions of this source code include the above copyright
>      notice, this list of conditions and the following disclaimer;
>
>   2. distributions in binary form include the above copyright
>      notice, this list of conditions and the following disclaimer
>      in the documentation and/or other associated materials;
>
>   3. the copyright holder's name is not used to endorse products
>      built using this software without specific written permission.
>
> ALTERNATIVELY, provided that this notice is retained in full, this product
> may be distributed under the terms of the GNU General Public License (GPL),
> in which case the provisions of the GPL apply INSTEAD OF those given above.
>
> DISCLAIMER
>
> This software is provided 'as is' with no explicit or implied warranties
> in respect of its properties, including, but not limited to, correctness
> and/or fitness for purpose.
>
> ---------------------------------------------------------------------------
*/

/**
* \file Hmac.h
*	Interface for the CHmac class.
*
*/

#if !defined(ZIPARCHIVE_HMAC_DOT_H)
#define ZIPARCHIVE_HMAC_DOT_H

#if _MSC_VER > 1000
	#pragma once
#endif // _MSC_VER > 1000

#include "Sha1.h"
#include "ZipAutoBuffer.h"



/**
	Implements HMAC - the FIPS standard keyed hash function.
 */
class ZIP_API CHmac
{
public:
	CHmac();
	CHmac(const CHmac& hmac)
	{
		*this = hmac;
	}
	CHmac& operator = (const CHmac& hmac)
	{
		memcpy(m_key, hmac.m_key, ZIP_SHA1_BLOCK_SIZE);
		m_uKeyLength = hmac.m_uKeyLength;
		m_sha1 = hmac.m_sha1;
		return *this;
	}
	/**
		Sets the HMAC key. Can be called multiple times.
		
		\param[in] pKey
			The key to set.
		
		\param uKeyLength
			The length of \a pKey.
		
		\returns
			\c false, if the total key input size is too large; \c true otherwise.
	 */
	bool SHA1Key(const char* pKey, UINT uKeyLength);

	/**
		Sets the HMAC data. Can be called multiple times.
		This method terminates the key input phase.
		
		\param[in] pData
			The HMAC data.
		
		\param uSize
			The size of \a pData.
	 */
	void Data(const char* pData, UINT uSize);

	/**
		Computes and outputs the MAC value.
		
		\param[out] pData
			The buffer to retrieve the result.
		
		\param uSize
			The size of \a pData.
	 */
	void End(char* pData, UINT uSize);
	~CHmac();
protected:
	char m_key[ZIP_SHA1_BLOCK_SIZE];
	UINT m_uKeyLength;
    CSha1 m_sha1;    
};

#endif

#endif
