////////////////////////////////////////////////////////////////////////////////
// This source file is part of the ZipArchive library source distribution and
// is Copyrighted 2000 - 2007 by Artpol Software - Tadeusz Dracz
//
// This source code is licensed as closed source and its use is 
// strictly subject to the terms and conditions of the 
// The ZipArchive Library Commercial License.
// The license is distributed with the source code in the License.txt file.
//
// Web Site: http://www.artpol-software.com
////////////////////////////////////////////////////////////////////////////////

#ifdef _ZIP_AES

/*
  
  ADDITIONAL INFORMATION

  The source code in this file is based on Dr Brian Gladman implementation of the AES algorithm,
  which at the moment of writing is freely available at http://fp.gladman.plus.com/AES/index.htm.
  The AES source code is reused under the permission granted by Dr Brian Gladman in original sources
  in a copyright notice which is reproduced below:
  

> ---------------------------------------------------------------------------
> Copyright (c) 2003, Dr Brian Gladman, Worcester, UK.   All rights reserved.
>
> LICENSE TERMS
>
> The free distribution and use of this software in both source and binary
> form is allowed (with or without changes) provided that:
>
>   1. distributions of this source code include the above copyright
>      notice, this list of conditions and the following disclaimer;
>
>   2. distributions in binary form include the above copyright
>      notice, this list of conditions and the following disclaimer
>      in the documentation and/or other associated materials;
>
>   3. the copyright holder's name is not used to endorse products
>      built using this software without specific written permission.
>
> ALTERNATIVELY, provided that this notice is retained in full, this product
> may be distributed under the terms of the GNU General Public License (GPL),
> in which case the provisions of the GPL apply INSTEAD OF those given above.
>
> DISCLAIMER
>
> This software is provided 'as is' with no explicit or implied warranties
> in respect of its properties, including, but not limited to, correctness
> and/or fitness for purpose.
>
> ---------------------------------------------------------------------------
*/

/**
* \file Aes.h
*	Interface for the CAes class.
*
*/

#if !defined(ZIPARCHIVE_AES_DOT_H)
#define ZIPARCHIVE_AES_DOT_H

#if _MSC_VER > 1000
	#pragma once
	#pragma warning( push )
	#pragma warning (disable : 4324) // 'CAes' : structure was padded due to __declspec(align())
#endif // _MSC_VER > 1000

#include "ZipAutoBuffer.h"

#if defined(_MSC_VER) && ( _MSC_VER >= 1300 )
	#define ZIP_AES_ALIGN __declspec(align(32))
#else
	#define ZIP_AES_ALIGN
#endif

#define ZIP_AES_BLOCK_SIZE 16
#define ZIP_AES_RC_LENGTH   (5 * (ZIP_AES_BLOCK_SIZE / 4 - 2))

/** 
	Implements AES encryption and decryption. The supported key lengths are: 128-bit, 192-bit and 256-bit.

	\see 
		<a href="kb">0610201627|aes</a>
 */
class ZIP_API CAes
{
public:
	/**
		Initializes a new instance of the CAes class.
	*/
	CAes()
	{
		Init();
	}

	/**
		Initializes a new instance of the CAes class.
		
		\param[in] pKey
			The key to use.
		
		\param uSize
			The size of \a pKey in bytes. Valid values are: 16, 24 or 32.
		
		\param bEncryption
			\c true if initialization is for encryption, \c false - for decryption.

		\throws CZipException
			with the CZipException::genericError code, if \a uSize is invalid.
	 */
	CAes(char* pKey, UINT uSize, bool bEncryption);

	/**
		Encrypts a data block of \c ZIP_AES_BLOCK_SIZE size.

		\param[in] input
			The data to encrypt.

		\param[out] output
			The encrypted data.

		\returns
			\c false if an internal check fails; \c true otherwise.
	 */
	bool EncryptBlock(const char* input, char* output);

	/**
		Decrypts a data block of \c ZIP_AES_BLOCK_SIZE size.
		
		\param[in] input
			The data to decrypt.
		
		\param[out] output
			The decrypted data.
		
		\returns
			\c false if an internal check fails; \c true otherwise.
	 */
	bool DecryptBlock(const char* input, char* output);

	/**
		Encrypts or decrypts a data block of a given size.
		
		\param[in,out] pData
			The buffer with data to encrypt or decrypt and to retrieve the processed data.
		
		\param uSize
			The size of \a pData.
	 */
	void Encrypt(char* pData, UINT uSize);	

	/**
		Sets the encryption key.
		
		\param[in] pKey
			The key to set.
		
		\param uSize
			The size of \a pKey in bytes. Valid values are: 16, 24 or 32.

		\throws CZipException
			with the CZipException::genericError code, if \a uSize is invalid.
	 */
	void SetEncryptKey(char* pKey, UINT uSize);

	/**
		Sets the decryption key.
		
		\param pKey
			The key to set.
		
		\param uSize
			The size of \a pKey in bytes. Valid values are: 16, 24 or 32.

		\throws CZipException
			with the CZipException::genericError code, if \a uSize is invalid.
	 */
	void SetDecryptKey(char* pKey, UINT uSize);
	~CAes(){}
private:		
	void Init();
	void SetEncryptKey128(const char* pKey);
	void SetEncryptKey192(const char* pKey);
	void SetEncryptKey256(const char* pKey);
	void SetDecryptKey128(const char* pKey);
	void SetDecryptKey192(const char* pKey);
	void SetDecryptKey256(const char* pKey);

	
		
	ZIP_AES_ALIGN char m_nonce[ZIP_AES_BLOCK_SIZE];
	ZIP_AES_ALIGN char m_buffer[ZIP_AES_BLOCK_SIZE];
	UINT m_uPosition;

	static bool GenerateTables();
	static ZIP_AES_ALIGN UINT m_fn[4][256];
	static ZIP_AES_ALIGN UINT m_fl[4][256];
	static ZIP_AES_ALIGN UINT m_in[4][256];
	static ZIP_AES_ALIGN UINT m_il[4][256];
	static ZIP_AES_ALIGN UINT m_im[4][256];
	static ZIP_AES_ALIGN UINT m_rc[ZIP_AES_RC_LENGTH];
	static bool m_bInit;
typedef union
{   
	UINT l;
    BYTE b[4];
} inf;

	UINT m_ks[60];
    inf m_inf;
};

#if _MSC_VER > 1000
	#pragma warning( pop )
#endif

#endif

#endif
