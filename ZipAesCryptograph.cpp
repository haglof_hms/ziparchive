////////////////////////////////////////////////////////////////////////////////
// This source file is part of the ZipArchive library source distribution and
// is Copyrighted 2000 - 2007 by Artpol Software - Tadeusz Dracz
//
// This source code is licensed as closed source and its use is 
// strictly subject to the terms and conditions of the 
// The ZipArchive Library Commercial License.
// The license is distributed with the source code in the License.txt file.
//
// Web Site: http://www.artpol-software.com
////////////////////////////////////////////////////////////////////////////////

#include "_features.h"

#ifdef _ZIP_AES

/*
  
  ADDITIONAL INFORMATION

  The source code in this file is based on Dr Brian Gladman implementation of the AES algorithm,
  which at the moment of writing is freely available at http://fp.gladman.plus.com/AES/index.htm.
  The AES source code is reused under the permission granted by Dr Brian Gladman in original sources
  in a copyright notice which is reproduced below:
  

> ---------------------------------------------------------------------------
> Copyright (c) 2003, Dr Brian Gladman, Worcester, UK.   All rights reserved.
>
> LICENSE TERMS
>
> The free distribution and use of this software in both source and binary
> form is allowed (with or without changes) provided that:
>
>   1. distributions of this source code include the above copyright
>      notice, this list of conditions and the following disclaimer;
>
>   2. distributions in binary form include the above copyright
>      notice, this list of conditions and the following disclaimer
>      in the documentation and/or other associated materials;
>
>   3. the copyright holder's name is not used to endorse products
>      built using this software without specific written permission.
>
> ALTERNATIVELY, provided that this notice is retained in full, this product
> may be distributed under the terms of the GNU General Public License (GPL),
> in which case the provisions of the GPL apply INSTEAD OF those given above.
>
> DISCLAIMER
>
> This software is provided 'as is' with no explicit or implied warranties
> in respect of its properties, including, but not limited to, correctness
> and/or fitness for purpose.
>
> ---------------------------------------------------------------------------
*/

#include "stdafx.h"
#include "ZipAesCryptograph.h"
#include "ZipException.h"

#define ZIP_AES_PWD_VERIFIER_SIZE 2

CRandomPool CZipAesCryptograph::m_pool;

CZipAesCryptograph::CZipAesCryptograph(int iStrength)
{
	if (iStrength < CZipCryptograph::encWinZipAes128 || iStrength > CZipCryptograph::encWinZipAes256)
		CZipException::Throw(CZipException::genericError);
	m_iStrength = iStrength;
	m_pAes = NULL;
}

DWORD CZipAesCryptograph::GetEncryptedInfoSize(int iStrength)
{
	return GetSaltSize(iStrength) + ZIP_AES_PWD_VERIFIER_SIZE + ZIP_AES_MAC_SIZE;
}

#if _MSC_VER > 1000
	#pragma warning( push )
	#pragma warning (disable : 4702) // unreachable code
#endif // _MSC_VER > 1000

int CZipAesCryptograph::GetSaltSize(int iStrength)
{
	switch(iStrength)
	{
		case CZipCryptograph::encWinZipAes128:
			return 8;
		case CZipCryptograph::encWinZipAes192:
			return 12;
		case CZipCryptograph::encWinZipAes256:
			return 16;
		default:
			CZipException::Throw(CZipException::genericError);
	}
	// some compilers complain if it is, some if it is not...
	return 0;
}


int CZipAesCryptograph::GetKeySize(int iStrength)
{
	switch(iStrength)
	{
		case CZipCryptograph::encWinZipAes128:
			return 16;
		case CZipCryptograph::encWinZipAes192:
			return 24;
		case CZipCryptograph::encWinZipAes256:
			return 32;
		default:
			CZipException::Throw(CZipException::genericError);
	}
	// some compilers complain if it is, some if it is not...
	return 0;
}

#if _MSC_VER > 1000
	#pragma warning( pop )
#endif // _MSC_VER > 1000

void CZipAesCryptograph::Init(const CZipAutoBuffer& password, const CZipAutoBuffer& salt, CZipAutoBuffer& verifier)
{
	const int uKeyLength = GetKeySize(m_iStrength);
	CZipAutoBuffer key;	
	key.Allocate(2 * uKeyLength + ZIP_AES_PWD_VERIFIER_SIZE);
	DeriveKey(password, salt, 1000, key);    	

    /* initialise for encryption using key 1            */
	m_pAes = new CAes(key, uKeyLength, true);

    /* initialise for authentication using key 2        */
	m_hmac.SHA1Key(key + uKeyLength, uKeyLength);
	verifier.Allocate(ZIP_AES_PWD_VERIFIER_SIZE);
	memcpy(verifier, key + key.GetSize() - ZIP_AES_PWD_VERIFIER_SIZE, ZIP_AES_PWD_VERIFIER_SIZE);
}


bool CZipAesCryptograph::InitDecode(CZipAutoBuffer& password, CZipFileHeader&, CZipStorage& storage)
{
	ClearAes();
	CZipAutoBuffer buf; // the salt
	buf.Allocate(GetSaltSize(m_iStrength), true);
	storage.Read(buf, buf.GetSize(), false);
	CZipAutoBuffer verifier;
	Init(password, buf, verifier);
	
	buf.Allocate(ZIP_AES_PWD_VERIFIER_SIZE);
	storage.Read(buf, ZIP_AES_PWD_VERIFIER_SIZE, false);	
	return memcmp(verifier, buf, ZIP_AES_PWD_VERIFIER_SIZE) == 0;
}

void CZipAesCryptograph::InitEncode(CZipAutoBuffer& password, CZipFileHeader& currentFile, CZipStorage& storage)
{
	ClearAes();
	// the salt should be unique for every file
	CZipAutoBuffer salt;
	salt.Allocate(GetSaltSize(m_iStrength), true);
	m_pool.GetRandom(salt);
	storage.Write(salt, salt.GetSize(), false);
	CZipAutoBuffer verifier;
	Init(password, salt, verifier);	
	currentFile.m_uComprSize = salt.GetSize() + verifier.GetSize();	
	storage.Write(verifier, verifier.GetSize(), false);
}

void CZipAesCryptograph::FinishEncode(CZipFileHeader& currentFile, CZipStorage& storage)
{
	ASSERT(m_pAes);
	CZipAutoBuffer mac;
	Finish(mac);
	storage.Write(mac, ZIP_AES_MAC_SIZE, false);
	currentFile.m_uComprSize += ZIP_AES_MAC_SIZE;
}

void CZipAesCryptograph::FinishDecode(CZipFileHeader&, CZipStorage& storage)
{
	ASSERT(m_pAes);
	CZipAutoBuffer mac;
	Finish(mac);
	CZipAutoBuffer buf;
	buf.Allocate(ZIP_AES_MAC_SIZE, true);
	storage.Read(buf, ZIP_AES_MAC_SIZE, false);
	if (memcmp(buf, mac, ZIP_AES_MAC_SIZE) != 0)
		CZipException::Throw(CZipException::badAesAuthCode);
}

void CZipAesCryptograph::DeriveKey(const CZipAutoBuffer& password, const CZipAutoBuffer& salt, UINT iIterations, CZipAutoBuffer& key)
{
    char uu[ZIP_SHA1_DIGEST_SIZE], ux[ZIP_SHA1_DIGEST_SIZE];
	CHmac c1;
	c1.SHA1Key(password, password.GetSize());
	CHmac c2(c1);
	c2.Data(salt, salt.GetSize());
	UINT uKeyLength = key.GetSize();
    /* find the number of SHA blocks in the pKey         */
    UINT nBlk = 1 + (uKeyLength - 1) / ZIP_SHA1_DIGEST_SIZE;
	char* pKey = key;
	
    for(UINT i = 0; i < nBlk; ++i) /* for each block in key */
    {
        /* ux[] holds the running xor value             */
        memset(ux, 0, ZIP_SHA1_DIGEST_SIZE);

        /* set HMAC context (c3) for password and salt  */
		CHmac c3(c2);
        

        /* enter additional data for 1st block into uu  */
        uu[0] = (BYTE)((i + 1) >> 24);
        uu[1] = (BYTE)((i + 1) >> 16);
        uu[2] = (BYTE)((i + 1) >> 8);
        uu[3] = (BYTE)(i + 1);

		UINT j, k;
        /* this is the key mixing iteration         */
        for(j = 0, k = 4; j < iIterations; ++j)
        {
			/* add previous round data to HMAC      */
			c3.Data(uu, k);

			/* obtain HMAC for uu[]                 */
			c3.End(uu, ZIP_SHA1_DIGEST_SIZE);

            /* xor into the running xor block       */
            for(k = 0; k < ZIP_SHA1_DIGEST_SIZE; ++k)
                ux[k] ^= uu[k];

            /* set HMAC context (c3) for password   */
			c3 = c1;            
        }

        /* compile key blocks into the key output   */
        j = 0; k = i * ZIP_SHA1_DIGEST_SIZE;
        while(j < ZIP_SHA1_DIGEST_SIZE && k < uKeyLength)
            pKey[k++] = ux[j++];
    }
}

#endif
