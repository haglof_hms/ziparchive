////////////////////////////////////////////////////////////////////////////////
// This source file is part of the ZipArchive library source distribution and
// is Copyrighted 2000 - 2007 by Artpol Software - Tadeusz Dracz
//
// This source code is licensed as closed source and its use is 
// strictly subject to the terms and conditions of the 
// The ZipArchive Library Commercial License.
// The license is distributed with the source code in the License.txt file.
//
// Web Site: http://www.artpol-software.com
////////////////////////////////////////////////////////////////////////////////

#ifdef _ZIP_AES

/*
  
  ADDITIONAL INFORMATION

  The source code in this file is based on Dr Brian Gladman implementation of the AES algorithm,
  which at the moment of writing is freely available at http://fp.gladman.plus.com/AES/index.htm.
  The AES source code is reused under the permission granted by Dr Brian Gladman in original sources
  in a copyright notice which is reproduced below:
  

> ---------------------------------------------------------------------------
> Copyright (c) 2003, Dr Brian Gladman, Worcester, UK.   All rights reserved.
>
> LICENSE TERMS
>
> The free distribution and use of this software in both source and binary
> form is allowed (with or without changes) provided that:
>
>   1. distributions of this source code include the above copyright
>      notice, this list of conditions and the following disclaimer;
>
>   2. distributions in binary form include the above copyright
>      notice, this list of conditions and the following disclaimer
>      in the documentation and/or other associated materials;
>
>   3. the copyright holder's name is not used to endorse products
>      built using this software without specific written permission.
>
> ALTERNATIVELY, provided that this notice is retained in full, this product
> may be distributed under the terms of the GNU General Public License (GPL),
> in which case the provisions of the GPL apply INSTEAD OF those given above.
>
> DISCLAIMER
>
> This software is provided 'as is' with no explicit or implied warranties
> in respect of its properties, including, but not limited to, correctness
> and/or fitness for purpose.
>
> ---------------------------------------------------------------------------
*/

/**
* \file Sha1.h
*	Interface for the CSha1 class.
*
*/

#if !defined(ZIPARCHIVE_SHA1_DOT_H)
#define ZIPARCHIVE_SHA1_DOT_H

#if _MSC_VER > 1000
	#pragma once
#endif // _MSC_VER > 1000

#include "ZipAutoBuffer.h"
#include "memory.h"

#define ZIP_SHA1_BLOCK_SIZE  64
#define ZIP_SHA1_DIGEST_SIZE 20


/**
	Implements a byte oriented version of SHA1 that operates on arrays of bytes stored in memory.
 */
class ZIP_API CSha1
{
	friend class CRandomPool;
public:
	CSha1();
	CSha1(const CSha1& sha1)
	{
		*this = sha1;
	}
	
	CSha1& operator = (const CSha1& sha1)
	{
		memcpy(m_count, sha1.m_count, 2 * sizeof(UINT));
		memcpy(m_hash, sha1.m_hash, 5 * sizeof(UINT));
		memcpy(m_wbuf, sha1.m_wbuf, 16 * sizeof(UINT));
		return *this;
	}
	/**
		Sets initial values.
	 */
	void Begin();

	/**
		Performs compilation.
	 */
	void Compile();	

	/**
		Hashes data with SHA1 in an array of bytes into hash buffer and 
		calls the #Compile method.
		
		\param[in] pData
			The data to hash.
		
		\param uSize
			The size of \a pData.
	 */
	void Hash(const char* pData, UINT uSize);

	/**
		Outputs the calculated hash value.
		
		\param[out] pData
			The buffer to retrieve results.
	 */
	void End(char* pData);
private:
	UINT m_count[2];
    UINT m_hash[5];
    UINT m_wbuf[16];	
};

#endif

#endif
