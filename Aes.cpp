////////////////////////////////////////////////////////////////////////////////
// This source file is part of the ZipArchive library source distribution and
// is Copyrighted 2000 - 2007 by Artpol Software - Tadeusz Dracz
//
// This source code is licensed as closed source and its use is 
// strictly subject to the terms and conditions of the 
// The ZipArchive Library Commercial License.
// The license is distributed with the source code in the License.txt file.
//
// Web Site: http://www.artpol-software.com
////////////////////////////////////////////////////////////////////////////////

#include "_features.h"

#ifdef _ZIP_AES

/*
  
  ADDITIONAL INFORMATION

  The source code in this file is based on Dr Brian Gladman implementation of the AES algorithm,
  which at the moment of writing is freely available at http://fp.gladman.plus.com/AES/index.htm.
  The AES source code is reused under the permission granted by Dr Brian Gladman in original sources
  in a copyright notice which is reproduced below:
  

> ---------------------------------------------------------------------------
> Copyright (c) 2003, Dr Brian Gladman, Worcester, UK.   All rights reserved.
>
> LICENSE TERMS
>
> The free distribution and use of this software in both source and binary
> form is allowed (with or without changes) provided that:
>
>   1. distributions of this source code include the above copyright
>      notice, this list of conditions and the following disclaimer;
>
>   2. distributions in binary form include the above copyright
>      notice, this list of conditions and the following disclaimer
>      in the documentation and/or other associated materials;
>
>   3. the copyright holder's name is not used to endorse products
>      built using this software without specific written permission.
>
> ALTERNATIVELY, provided that this notice is retained in full, this product
> may be distributed under the terms of the GNU General Public License (GPL),
> in which case the provisions of the GPL apply INSTEAD OF those given above.
>
> DISCLAIMER
>
> This software is provided 'as is' with no explicit or implied warranties
> in respect of its properties, including, but not limited to, correctness
> and/or fitness for purpose.
>
> ---------------------------------------------------------------------------
*/

#include "stdafx.h"

#include "Aes.h"
#include "ZipMemFile.h"
#include "ZipException.h"
#include "BytesWriter.h"

#if _MSC_VER > 1000
	#pragma warning (push)
	#pragma warning (disable : 4709) // comma operator within array index expression
#endif // _MSC_VER > 1000

#define N_COLS 4 
#define AES_ARRAYS
#define AES_REV_DKS

#ifdef ZIP_ARCHIVE_LITTLE_ENDIAN
	#define upr(x,n)        (((UINT)(x) << (8 * (n))) | ((UINT)(x) >> (32 - 8 * (n))))
	#define ups(x,n)        ((UINT) (x) << (8 * (n)))
	#define bval(x,n)       ((BYTE)(((x) >> (8 * (n)))&0xFF))
	#define bytes2word(b0, b1, b2, b3)  \
			(((UINT)(b3) << 24) | ((UINT)(b2) << 16) | ((UINT)(b1) << 8) | (b0))
#else
	#define upr(x,n)        (((UINT)(x) >> (8 * (n))) | ((UINT)(x) << (32 - 8 * (n))))
	#define ups(x,n)        ((UINT) (x) >> (8 * (n)))
	#define bval(x,n)       ((BYTE)(((x) >> (24 - 8 * (n)))&0xFF))
	#define bytes2word(b0, b1, b2, b3)  \
        (((UINT)(b0) << 24) | ((UINT)(b1) << 16) | ((UINT)(b2) << 8) | (b3))
#endif

#ifndef _MSC_VER
	#define word_in(x,c)    bytes2word(((const BYTE*)(x)+4*c)[0], ((const BYTE*)(x)+4*c)[1], \
                                   ((const BYTE*)(x)+4*c)[2], ((const BYTE*)(x)+4*c)[3])
	#define word_out(x,c,v) { ((BYTE*)(x)+4*c)[0] = bval(v,0); ((BYTE*)(x)+4*c)[1] = bval(v,1); \
                          ((BYTE*)(x)+4*c)[2] = bval(v,2); ((BYTE*)(x)+4*c)[3] = bval(v,3); }
#else
	#define word_in(x,c)    (*((UINT*)(x)+(c)))
	#define word_out(x,c,v) (*((UINT*)(x)+(c)) = (v))
#endif

#define WPOLY   0x011b

#define f2(x) ((x) ? pow[log[x] + 0x19] : 0)
#define f3(x) ((x) ? pow[log[x] + 0x01] : 0)
#define f9(x) ((x) ? pow[log[x] + 0xc7] : 0)
#define fb(x) ((x) ? pow[log[x] + 0x68] : 0)
#define fd(x) ((x) ? pow[log[x] + 0xee] : 0)
#define fe(x) ((x) ? pow[log[x] + 0xdf] : 0)
#define fi(x) ((x) ? pow[ 255 - log[x]] : 0)

#define t_set(m,n) m_##m##n
#define t_use(m,n) m_##m##n

#define fwd_affine(x) \
    (w = (UINT)x, w ^= (w<<1)^(w<<2)^(w<<3)^(w<<4), 0x63^(BYTE)((w^(w>>8))&0xFF))

#define inv_affine(x) \
    (w = (UINT)x, w = (w<<1)^(w<<3)^(w<<6), 0x05^(BYTE)((w^(w>>8))&0xFF))

#define four_tables(x,tab,vf,rf,c) \
 (  tab[0][bval(vf(x,0,c),rf(0,c))] \
  ^ tab[1][bval(vf(x,1,c),rf(1,c))] \
  ^ tab[2][bval(vf(x,2,c),rf(2,c))] \
  ^ tab[3][bval(vf(x,3,c),rf(3,c))])

#define vf1(x,r,c)  (x)
#define rf1(r,c)    (r)
#define rf2(r,c)    ((8+r-c)&3)

#define ls_box(x,c)       four_tables(x,t_use(f,l),vf1,rf2,c)
#define inv_mcol(x)       four_tables(x,t_use(i,m),vf1,rf1,0)

#define ke4(k,i) \
{   k[4*(i)+4] = ss[0] ^= ls_box(ss[3],3) ^ t_use(r,c)[i]; \
    k[4*(i)+5] = ss[1] ^= ss[0]; \
    k[4*(i)+6] = ss[2] ^= ss[1]; \
    k[4*(i)+7] = ss[3] ^= ss[2]; \
}

#define kef6(k,i) \
{   k[6*(i)+ 6] = ss[0] ^= ls_box(ss[5],3) ^ t_use(r,c)[i]; \
    k[6*(i)+ 7] = ss[1] ^= ss[0]; \
    k[6*(i)+ 8] = ss[2] ^= ss[1]; \
    k[6*(i)+ 9] = ss[3] ^= ss[2]; \
}

#define ke6(k,i) \
{   kef6(k,i); \
    k[6*(i)+10] = ss[4] ^= ss[3]; \
    k[6*(i)+11] = ss[5] ^= ss[4]; \
}

#define kef8(k,i) \
{   k[8*(i)+ 8] = ss[0] ^= ls_box(ss[7],3) ^ t_use(r,c)[i]; \
    k[8*(i)+ 9] = ss[1] ^= ss[0]; \
    k[8*(i)+10] = ss[2] ^= ss[1]; \
    k[8*(i)+11] = ss[3] ^= ss[2]; \
}

#define ke8(k,i) \
{   kef8(k,i); \
    k[8*(i)+12] = ss[4] ^= ls_box(ss[3],0); \
    k[8*(i)+13] = ss[5] ^= ss[4]; \
    k[8*(i)+14] = ss[6] ^= ss[5]; \
    k[8*(i)+15] = ss[7] ^= ss[6]; \
}

#define v(n,i)  ((n) - (i) + 2 * ((i) & 3))
#define ff(x)   inv_mcol(x)

#define k4e(k,i) \
{   k[v(40,(4*(i))+4)] = ss[0] ^= ls_box(ss[3],3) ^ t_use(r,c)[i]; \
    k[v(40,(4*(i))+5)] = ss[1] ^= ss[0]; \
    k[v(40,(4*(i))+6)] = ss[2] ^= ss[1]; \
    k[v(40,(4*(i))+7)] = ss[3] ^= ss[2]; \
}

#define kdf4(k,i) \
{   ss[0] = ss[0] ^ ss[2] ^ ss[1] ^ ss[3]; \
    ss[1] = ss[1] ^ ss[3]; \
    ss[2] = ss[2] ^ ss[3]; \
    ss[4] = ls_box(ss[(i+3) % 4], 3) ^ t_use(r,c)[i]; \
    ss[i % 4] ^= ss[4]; \
    ss[4] ^= k[v(40,(4*(i)))];   k[v(40,(4*(i))+4)] = ff(ss[4]); \
    ss[4] ^= k[v(40,(4*(i))+1)]; k[v(40,(4*(i))+5)] = ff(ss[4]); \
    ss[4] ^= k[v(40,(4*(i))+2)]; k[v(40,(4*(i))+6)] = ff(ss[4]); \
    ss[4] ^= k[v(40,(4*(i))+3)]; k[v(40,(4*(i))+7)] = ff(ss[4]); \
}

#define kd4(k,i) \
{   ss[4] = ls_box(ss[(i+3) % 4], 3) ^ t_use(r,c)[i]; \
    ss[i % 4] ^= ss[4]; ss[4] = ff(ss[4]); \
    k[v(40,(4*(i))+4)] = ss[4] ^= k[v(40,(4*(i)))]; \
    k[v(40,(4*(i))+5)] = ss[4] ^= k[v(40,(4*(i))+1)]; \
    k[v(40,(4*(i))+6)] = ss[4] ^= k[v(40,(4*(i))+2)]; \
    k[v(40,(4*(i))+7)] = ss[4] ^= k[v(40,(4*(i))+3)]; \
}

#define kdl4(k,i) \
{   ss[4] = ls_box(ss[(i+3) % 4], 3) ^ t_use(r,c)[i]; ss[i % 4] ^= ss[4]; \
    k[v(40,(4*(i))+4)] = (ss[0] ^= ss[1]) ^ ss[2] ^ ss[3]; \
    k[v(40,(4*(i))+5)] = ss[1] ^ ss[3]; \
    k[v(40,(4*(i))+6)] = ss[0]; \
    k[v(40,(4*(i))+7)] = ss[1]; \
}

#define k6ef(k,i) \
{   k[v(48,(6*(i))+ 6)] = ss[0] ^= ls_box(ss[5],3) ^ t_use(r,c)[i]; \
    k[v(48,(6*(i))+ 7)] = ss[1] ^= ss[0]; \
    k[v(48,(6*(i))+ 8)] = ss[2] ^= ss[1]; \
    k[v(48,(6*(i))+ 9)] = ss[3] ^= ss[2]; \
}

#define k6e(k,i) \
{   k6ef(k,i); \
    k[v(48,(6*(i))+10)] = ss[4] ^= ss[3]; \
    k[v(48,(6*(i))+11)] = ss[5] ^= ss[4]; \
}

#define kdf6(k,i) \
{   ss[0] ^= ls_box(ss[5],3) ^ t_use(r,c)[i]; k[v(48,(6*(i))+ 6)] = ff(ss[0]); \
    ss[1] ^= ss[0]; k[v(48,(6*(i))+ 7)] = ff(ss[1]); \
    ss[2] ^= ss[1]; k[v(48,(6*(i))+ 8)] = ff(ss[2]); \
    ss[3] ^= ss[2]; k[v(48,(6*(i))+ 9)] = ff(ss[3]); \
    ss[4] ^= ss[3]; k[v(48,(6*(i))+10)] = ff(ss[4]); \
    ss[5] ^= ss[4]; k[v(48,(6*(i))+11)] = ff(ss[5]); \
}

#define kd6(k,i) \
{   ss[6] = ls_box(ss[5],3) ^ t_use(r,c)[i]; \
    ss[0] ^= ss[6]; ss[6] = ff(ss[6]); k[v(48,(6*(i))+ 6)] = ss[6] ^= k[v(48,(6*(i)))]; \
    ss[1] ^= ss[0]; k[v(48,(6*(i))+ 7)] = ss[6] ^= k[v(48,(6*(i))+ 1)]; \
    ss[2] ^= ss[1]; k[v(48,(6*(i))+ 8)] = ss[6] ^= k[v(48,(6*(i))+ 2)]; \
    ss[3] ^= ss[2]; k[v(48,(6*(i))+ 9)] = ss[6] ^= k[v(48,(6*(i))+ 3)]; \
    ss[4] ^= ss[3]; k[v(48,(6*(i))+10)] = ss[6] ^= k[v(48,(6*(i))+ 4)]; \
    ss[5] ^= ss[4]; k[v(48,(6*(i))+11)] = ss[6] ^= k[v(48,(6*(i))+ 5)]; \
}

#define kdl6(k,i) \
{   ss[0] ^= ls_box(ss[5],3) ^ t_use(r,c)[i]; k[v(48,(6*(i))+ 6)] = ss[0]; \
    ss[1] ^= ss[0]; k[v(48,(6*(i))+ 7)] = ss[1]; \
    ss[2] ^= ss[1]; k[v(48,(6*(i))+ 8)] = ss[2]; \
    ss[3] ^= ss[2]; k[v(48,(6*(i))+ 9)] = ss[3]; \
}

#define k8ef(k,i) \
{   k[v(56,(8*(i))+ 8)] = ss[0] ^= ls_box(ss[7],3) ^ t_use(r,c)[i]; \
    k[v(56,(8*(i))+ 9)] = ss[1] ^= ss[0]; \
    k[v(56,(8*(i))+10)] = ss[2] ^= ss[1]; \
    k[v(56,(8*(i))+11)] = ss[3] ^= ss[2]; \
}

#define k8e(k,i) \
{   k8ef(k,i); \
    k[v(56,(8*(i))+12)] = ss[4] ^= ls_box(ss[3],0); \
    k[v(56,(8*(i))+13)] = ss[5] ^= ss[4]; \
    k[v(56,(8*(i))+14)] = ss[6] ^= ss[5]; \
    k[v(56,(8*(i))+15)] = ss[7] ^= ss[6]; \
}

#define kdf8(k,i) \
{   ss[0] ^= ls_box(ss[7],3) ^ t_use(r,c)[i]; k[v(56,(8*(i))+ 8)] = ff(ss[0]); \
    ss[1] ^= ss[0]; k[v(56,(8*(i))+ 9)] = ff(ss[1]); \
    ss[2] ^= ss[1]; k[v(56,(8*(i))+10)] = ff(ss[2]); \
    ss[3] ^= ss[2]; k[v(56,(8*(i))+11)] = ff(ss[3]); \
    ss[4] ^= ls_box(ss[3],0); k[v(56,(8*(i))+12)] = ff(ss[4]); \
    ss[5] ^= ss[4]; k[v(56,(8*(i))+13)] = ff(ss[5]); \
    ss[6] ^= ss[5]; k[v(56,(8*(i))+14)] = ff(ss[6]); \
    ss[7] ^= ss[6]; k[v(56,(8*(i))+15)] = ff(ss[7]); \
}

#define kd8(k,i) \
{   ss[8] = ls_box(ss[7],3) ^ t_use(r,c)[i]; \
    ss[0] ^= ss[8]; ss[8] = ff(ss[8]); k[v(56,(8*(i))+ 8)] = ss[8] ^= k[v(56,(8*(i)))]; \
    ss[1] ^= ss[0]; k[v(56,(8*(i))+ 9)] = ss[8] ^= k[v(56,(8*(i))+ 1)]; \
    ss[2] ^= ss[1]; k[v(56,(8*(i))+10)] = ss[8] ^= k[v(56,(8*(i))+ 2)]; \
    ss[3] ^= ss[2]; k[v(56,(8*(i))+11)] = ss[8] ^= k[v(56,(8*(i))+ 3)]; \
    ss[8] = ls_box(ss[3],0); \
    ss[4] ^= ss[8]; ss[8] = ff(ss[8]); k[v(56,(8*(i))+12)] = ss[8] ^= k[v(56,(8*(i))+ 4)]; \
    ss[5] ^= ss[4]; k[v(56,(8*(i))+13)] = ss[8] ^= k[v(56,(8*(i))+ 5)]; \
    ss[6] ^= ss[5]; k[v(56,(8*(i))+14)] = ss[8] ^= k[v(56,(8*(i))+ 6)]; \
    ss[7] ^= ss[6]; k[v(56,(8*(i))+15)] = ss[8] ^= k[v(56,(8*(i))+ 7)]; \
}

#define kdl8(k,i) \
{   ss[0] ^= ls_box(ss[7],3) ^ t_use(r,c)[i]; k[v(56,(8*(i))+ 8)] = ss[0]; \
    ss[1] ^= ss[0]; k[v(56,(8*(i))+ 9)] = ss[1]; \
    ss[2] ^= ss[1]; k[v(56,(8*(i))+10)] = ss[2]; \
    ss[3] ^= ss[2]; k[v(56,(8*(i))+11)] = ss[3]; \
}

#if defined(AES_ARRAYS)
	#define locals(y,x)     x[4],y[4]
	#define s(x,c) x[c]
#else
	#define locals(y,x)     x##0,x##1,x##2,x##3,y##0,y##1,y##2,y##3
	#define s(x,c) x##c
#endif

#define si(y,x,k,c) (s(y,c) = word_in(x, c) ^ (k)[c])
#define so(y,x,c)   word_out(y, c, s(x,c))
#define state_in(y,x,k) si(y,x,k,0); si(y,x,k,1); si(y,x,k,2); si(y,x,k,3)
#define state_out(y,x)  so(y,x,0); so(y,x,1); so(y,x,2); so(y,x,3)
#define round(rm,y,x,k) rm(y,x,k,0); rm(y,x,k,1); rm(y,x,k,2); rm(y,x,k,3)

#define fwd_var(x,r,c)\
 ( r == 0 ? ( c == 0 ? s(x,0) : c == 1 ? s(x,1) : c == 2 ? s(x,2) : s(x,3))\
 : r == 1 ? ( c == 0 ? s(x,1) : c == 1 ? s(x,2) : c == 2 ? s(x,3) : s(x,0))\
 : r == 2 ? ( c == 0 ? s(x,2) : c == 1 ? s(x,3) : c == 2 ? s(x,0) : s(x,1))\
 :          ( c == 0 ? s(x,3) : c == 1 ? s(x,0) : c == 2 ? s(x,1) : s(x,2)))

#define fwd_rnd(y,x,k,c)    (s(y,c) = (k)[c] ^ four_tables(x,t_use(f,n),fwd_var,rf1,c))
#define fwd_lrnd(y,x,k,c)   (s(y,c) = (k)[c] ^ four_tables(x,t_use(f,l),fwd_var,rf1,c))

#ifdef AES_REV_DKS
	#define key_ofs     0
	#define rnd_key(n)  (kp + n * N_COLS)
#else
	#define key_ofs     1
	#define rnd_key(n)  (kp - n * N_COLS)
#endif

#define inv_var(x,r,c)\
 ( r == 0 ? ( c == 0 ? s(x,0) : c == 1 ? s(x,1) : c == 2 ? s(x,2) : s(x,3))\
 : r == 1 ? ( c == 0 ? s(x,3) : c == 1 ? s(x,0) : c == 2 ? s(x,1) : s(x,2))\
 : r == 2 ? ( c == 0 ? s(x,2) : c == 1 ? s(x,3) : c == 2 ? s(x,0) : s(x,1))\
 :          ( c == 0 ? s(x,1) : c == 1 ? s(x,2) : c == 2 ? s(x,3) : s(x,0)))

#define inv_rnd(y,x,k,c)    (s(y,c) = (k)[c] ^ four_tables(x,t_use(i,n),inv_var,rf1,c))
#define inv_lrnd(y,x,k,c)   (s(y,c) = (k)[c] ^ four_tables(x,t_use(i,l),inv_var,rf1,c))

UINT CAes::m_fn[4][256];
UINT CAes::m_fl[4][256];
UINT CAes::m_in[4][256];
UINT CAes::m_il[4][256];
UINT CAes::m_im[4][256];
UINT CAes::m_rc[ZIP_AES_RC_LENGTH];
bool CAes::m_bInit = CAes::GenerateTables();

CAes::CAes(char* pKey, UINT uSize, bool bEncryption)
{	
	if (bEncryption)
		SetEncryptKey(pKey, uSize);
	else
		SetDecryptKey(pKey, uSize);
	Init();
}

void CAes::Init()
{
	m_uPosition = ZIP_AES_BLOCK_SIZE;
	memset(m_nonce, 0, ZIP_AES_BLOCK_SIZE);
}

bool CAes::GenerateTables()
{
    BYTE pow[512], log[256];
	UINT i = 0, w = 1;
    do
    {
        pow[i] = (BYTE)w;
        pow[i + 255] = (BYTE)w;
        log[w] = (BYTE)i++;
        w ^=  (w << 1) ^ (w & 0x80 ? WPOLY : 0);
    }
    while (w != 1);

	for(i = 0, w = 1; i < ZIP_AES_RC_LENGTH; ++i)
    {
        t_set(r,c)[i] = bytes2word(w, 0, 0, 0);
        w = f2(w);
    }

    for(i = 0; i < 256; ++i)
    {  
        BYTE b = (BYTE)fwd_affine(fi((BYTE)i));
        w = bytes2word(f2(b), b, b, f3(b));
		t_set(f,n)[0][i] = w;
        t_set(f,n)[1][i] = upr(w,1);
        t_set(f,n)[2][i] = upr(w,2);
        t_set(f,n)[3][i] = upr(w,3);
        w = bytes2word(b, 0, 0, 0);
		t_set(f,l)[0][i] = w;
        t_set(f,l)[1][i] = upr(w,1);
        t_set(f,l)[2][i] = upr(w,2);
        t_set(f,l)[3][i] = upr(w,3);
		b = (BYTE)fi(inv_affine((BYTE)i));
        w = bytes2word(fe(b), f9(b), fd(b), fb(b));
		t_set(i,m)[0][b] = w;
        t_set(i,m)[1][b] = upr(w,1);
        t_set(i,m)[2][b] = upr(w,2);
        t_set(i,m)[3][b] = upr(w,3);
		t_set(i,n)[0][i] = w;
        t_set(i,n)[1][i] = upr(w,1);
        t_set(i,n)[2][i] = upr(w,2);
        t_set(i,n)[3][i] = upr(w,3);
		w = bytes2word(b, 0, 0, 0);
		t_set(i,l)[0][i] = w;
        t_set(i,l)[1][i] = upr(w,1);
        t_set(i,l)[2][i] = upr(w,2);
        t_set(i,l)[3][i] = upr(w,3);
	}
	return true;
}

void CAes::SetEncryptKey(char* pKey, UINT uSize)
{
	switch (uSize)
	{
		case 16:
			SetEncryptKey128(pKey);
			break;
		case 24:
			SetEncryptKey192(pKey);
			break;
		case 32:
			SetEncryptKey256(pKey);
			break;
		default:
			CZipException::Throw(CZipException::genericError);
	}
}

void CAes::SetEncryptKey128(const char* pKey)
{
	UINT ss[4];

    m_ks[0] = ss[0] = word_in(pKey, 0);
    m_ks[1] = ss[1] = word_in(pKey, 1);
    m_ks[2] = ss[2] = word_in(pKey, 2);
    m_ks[3] = ss[3] = word_in(pKey, 3);
	ke4(m_ks, 0);  ke4(m_ks, 1);
    ke4(m_ks, 2);  ke4(m_ks, 3);
    ke4(m_ks, 4);  ke4(m_ks, 5);
    ke4(m_ks, 6);  ke4(m_ks, 7);
    ke4(m_ks, 8);
    ke4(m_ks, 9);
    m_inf.l = 0;
    m_inf.b[0] = 10 * 16;
}

void CAes::SetEncryptKey192(const char* pKey)
{
	UINT ss[6];

    m_ks[0] = ss[0] = word_in(pKey, 0);
    m_ks[1] = ss[1] = word_in(pKey, 1);
    m_ks[2] = ss[2] = word_in(pKey, 2);
    m_ks[3] = ss[3] = word_in(pKey, 3);
    m_ks[4] = ss[4] = word_in(pKey, 4);
    m_ks[5] = ss[5] = word_in(pKey, 5);
	ke6(m_ks, 0);  ke6(m_ks, 1);
    ke6(m_ks, 2);  ke6(m_ks, 3);
    ke6(m_ks, 4);  ke6(m_ks, 5);
    ke6(m_ks, 6);
    kef6(m_ks, 7);
    m_inf.l = 0;
    m_inf.b[0] = 12 * 16;
}

void CAes::SetEncryptKey256(const char* pKey)
{
	UINT ss[8];

    m_ks[0] = ss[0] = word_in(pKey, 0);
    m_ks[1] = ss[1] = word_in(pKey, 1);
    m_ks[2] = ss[2] = word_in(pKey, 2);
    m_ks[3] = ss[3] = word_in(pKey, 3);
    m_ks[4] = ss[4] = word_in(pKey, 4);
    m_ks[5] = ss[5] = word_in(pKey, 5);
    m_ks[6] = ss[6] = word_in(pKey, 6);
    m_ks[7] = ss[7] = word_in(pKey, 7);
	ke8(m_ks, 0); ke8(m_ks, 1);
    ke8(m_ks, 2); ke8(m_ks, 3);
    ke8(m_ks, 4); ke8(m_ks, 5);
    kef8(m_ks, 6);
    m_inf.l = 0;
    m_inf.b[0] = 14 * 16;
}

void CAes::SetDecryptKey(char* pKey, UINT uSize)
{
	switch (uSize)
	{
		case 16:
			SetDecryptKey128(pKey);
			break;
		case 24:
			SetDecryptKey192(pKey);
			break;
		case 32:
			SetDecryptKey256(pKey);
			break;
		default:
			CZipException::Throw(CZipException::genericError);
	}
}

void CAes::SetDecryptKey128(const char* pKey)
{
	UINT ss[5];
    m_ks[v(40,(0))] = ss[0] = word_in(pKey, 0);
    m_ks[v(40,(1))] = ss[1] = word_in(pKey, 1);
    m_ks[v(40,(2))] = ss[2] = word_in(pKey, 2);
    m_ks[v(40,(3))] = ss[3] = word_in(pKey, 3);
	kdf4(m_ks, 0);  kd4(m_ks, 1);
    kd4(m_ks, 2);  kd4(m_ks, 3);
    kd4(m_ks, 4);  kd4(m_ks, 5);
    kd4(m_ks, 6);  kd4(m_ks, 7);
    kd4(m_ks, 8); kdl4(m_ks, 9);
    m_inf.l = 0;
    m_inf.b[0] = 10 * 16;
}

void CAes::SetDecryptKey192(const char* pKey)
{
	UINT ss[7];
    m_ks[v(48,(0))] = ss[0] = word_in(pKey, 0);
    m_ks[v(48,(1))] = ss[1] = word_in(pKey, 1);
    m_ks[v(48,(2))] = ss[2] = word_in(pKey, 2);
    m_ks[v(48,(3))] = ss[3] = word_in(pKey, 3);
	m_ks[v(48,(4))] = ff(ss[4] = word_in(pKey, 4));
    m_ks[v(48,(5))] = ff(ss[5] = word_in(pKey, 5));
    kdf6(m_ks, 0); kd6(m_ks, 1);
    kd6(m_ks, 2);  kd6(m_ks, 3);
    kd6(m_ks, 4);  kd6(m_ks, 5);
    kd6(m_ks, 6); kdl6(m_ks, 7);
    m_inf.l = 0;
    m_inf.b[0] = 12 * 16;
}

void CAes::SetDecryptKey256(const char* pKey)
{
	UINT ss[9];
    m_ks[v(56,(0))] = ss[0] = word_in(pKey, 0);
    m_ks[v(56,(1))] = ss[1] = word_in(pKey, 1);
    m_ks[v(56,(2))] = ss[2] = word_in(pKey, 2);
    m_ks[v(56,(3))] = ss[3] = word_in(pKey, 3);
	m_ks[v(56,(4))] = ff(ss[4] = word_in(pKey, 4));
    m_ks[v(56,(5))] = ff(ss[5] = word_in(pKey, 5));
    m_ks[v(56,(6))] = ff(ss[6] = word_in(pKey, 6));
    m_ks[v(56,(7))] = ff(ss[7] = word_in(pKey, 7));
    kdf8(m_ks, 0); kd8(m_ks, 1);
    kd8(m_ks, 2);  kd8(m_ks, 3);
    kd8(m_ks, 4);  kd8(m_ks, 5);
    kdl8(m_ks, 6);
    m_inf.l = 0;
    m_inf.b[0] = 14 * 16;
}

void CAes::Encrypt(char* pData, UINT uSize)
{
	UINT i = 0;

    while(i < uSize)
    {
		if(m_uPosition >= ZIP_AES_BLOCK_SIZE)
		{
			UINT j = 0;
            /* increment encryption nonce   */
            while(j < 8 && !++m_nonce[j])
                ++j;
            /* encrypt the nonce to form next xor buffer    */
			EncryptBlock(m_nonce, m_buffer);
            m_uPosition = 0;
        }
		pData[i++] ^= m_buffer[m_uPosition++];
    }
}

/* Visual C++ .Net v7.1 provides the fastest encryption code when using
   Pentium optimiation with small code but this is poor for decryption
   so we need to control this with the following VC++ pragmas
*/
#if defined( _MSC_VER ) && !defined( _WIN64 ) && !defined( _DEBUG )
	#define _ZIP_AES_OPTIMIZE
#else 
	#ifdef _ZIP_AES_OPTIMIZE
		#undef _ZIP_AES_OPTIMIZE
	#endif
#endif

#ifdef _ZIP_AES_OPTIMIZE
	#pragma optimize( "s", on )
#endif

bool CAes::EncryptBlock(const char* input, char* output)
{
	if( m_inf.b[0] != 10 * 16 && m_inf.b[0] != 12 * 16 && m_inf.b[0] != 14 * 16 )
		return false;
	UINT locals(b0, b1);
    const UINT *kp = m_ks;
	state_in(b0, input, kp);

	switch(m_inf.b[0])
    {
    case 14 * 16:
        round(fwd_rnd,  b1, b0, kp + 1 * N_COLS);
        round(fwd_rnd,  b0, b1, kp + 2 * N_COLS);
        kp += 2 * N_COLS;
    case 12 * 16:
        round(fwd_rnd,  b1, b0, kp + 1 * N_COLS);
        round(fwd_rnd,  b0, b1, kp + 2 * N_COLS);
        kp += 2 * N_COLS;
    case 10 * 16:
        round(fwd_rnd,  b1, b0, kp + 1 * N_COLS);
        round(fwd_rnd,  b0, b1, kp + 2 * N_COLS);
        round(fwd_rnd,  b1, b0, kp + 3 * N_COLS);
        round(fwd_rnd,  b0, b1, kp + 4 * N_COLS);
        round(fwd_rnd,  b1, b0, kp + 5 * N_COLS);
        round(fwd_rnd,  b0, b1, kp + 6 * N_COLS);
        round(fwd_rnd,  b1, b0, kp + 7 * N_COLS);
        round(fwd_rnd,  b0, b1, kp + 8 * N_COLS);
        round(fwd_rnd,  b1, b0, kp + 9 * N_COLS);
        round(fwd_lrnd, b0, b1, kp +10 * N_COLS);
    }
	state_out(output, b0);
	return true;
}

#ifdef _ZIP_AES_OPTIMIZE
	#pragma optimize( "t", on )
#endif

bool CAes::DecryptBlock(const char* input, char* output)
{
	if( m_inf.b[0] != 10 * 16 && m_inf.b[0] != 12 * 16 && m_inf.b[0] != 14 * 16 )
		return false;
	UINT locals(b0, b1);
    const UINT *kp = m_ks + (key_ofs ? (m_inf.b[0] >> 2) : 0);
    state_in(b0, input, kp);
	kp = m_ks + (key_ofs ? 0 : (m_inf.b[0] >> 2));
    switch(m_inf.b[0])
    {
    case 14 * 16:
        round(inv_rnd,  b1, b0, rnd_key(-13));
        round(inv_rnd,  b0, b1, rnd_key(-12));
    case 12 * 16:
        round(inv_rnd,  b1, b0, rnd_key(-11));
        round(inv_rnd,  b0, b1, rnd_key(-10));
    case 10 * 16:
        round(inv_rnd,  b1, b0, rnd_key(-9));
        round(inv_rnd,  b0, b1, rnd_key(-8));
        round(inv_rnd,  b1, b0, rnd_key(-7));
        round(inv_rnd,  b0, b1, rnd_key(-6));
        round(inv_rnd,  b1, b0, rnd_key(-5));
        round(inv_rnd,  b0, b1, rnd_key(-4));
        round(inv_rnd,  b1, b0, rnd_key(-3));
        round(inv_rnd,  b0, b1, rnd_key(-2));
        round(inv_rnd,  b1, b0, rnd_key(-1));
        round(inv_lrnd, b0, b1, rnd_key( 0));
	}
	state_out(output, b0);
	return true;
}

#ifdef _ZIP_AES_OPTIMIZE
	#pragma optimize( "", on )
#endif

#if _MSC_VER > 1000
	#pragma warning (pop)
#endif // _MSC_VER > 1000

#endif
