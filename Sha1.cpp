////////////////////////////////////////////////////////////////////////////////
// This source file is part of the ZipArchive library source distribution and
// is Copyrighted 2000 - 2007 by Artpol Software - Tadeusz Dracz
//
// This source code is licensed as closed source and its use is 
// strictly subject to the terms and conditions of the 
// The ZipArchive Library Commercial License.
// The license is distributed with the source code in the License.txt file.
//
// Web Site: http://www.artpol-software.com
////////////////////////////////////////////////////////////////////////////////

#include "_features.h"

#ifdef _ZIP_AES

/*
  
  ADDITIONAL INFORMATION

  The source code in this file is based on Dr Brian Gladman implementation of the AES algorithm,
  which at the moment of writing is freely available at http://fp.gladman.plus.com/AES/index.htm.
  The AES source code is reused under the permission granted by Dr Brian Gladman in original sources
  in a copyright notice which is reproduced below:
  

> ---------------------------------------------------------------------------
> Copyright (c) 2003, Dr Brian Gladman, Worcester, UK.   All rights reserved.
>
> LICENSE TERMS
>
> The free distribution and use of this software in both source and binary
> form is allowed (with or without changes) provided that:
>
>   1. distributions of this source code include the above copyright
>      notice, this list of conditions and the following disclaimer;
>
>   2. distributions in binary form include the above copyright
>      notice, this list of conditions and the following disclaimer
>      in the documentation and/or other associated materials;
>
>   3. the copyright holder's name is not used to endorse products
>      built using this software without specific written permission.
>
> ALTERNATIVELY, provided that this notice is retained in full, this product
> may be distributed under the terms of the GNU General Public License (GPL),
> in which case the provisions of the GPL apply INSTEAD OF those given above.
>
> DISCLAIMER
>
> This software is provided 'as is' with no explicit or implied warranties
> in respect of its properties, including, but not limited to, correctness
> and/or fitness for purpose.
>
> ---------------------------------------------------------------------------
*/

#include "stdafx.h"
#include "Sha1.h"
#include "ZipCompatibility.h"


#define rotl32(x,n) (((x) << n) | ((x) >> (32 - n)))

/* A normal version as set out in the FIPS  */
#define rnd(f,k)    \
    t = a; a = rotl32(a,5) + f(b,c,d) + e + k + w[i]; \
    e = d; d = c; c = rotl32(b, 30); b = t

#define ch(x,y,z)       (((x) & (y)) ^ (~(x) & (z)))
#define parity(x,y,z)   ((x) ^ (y) ^ (z))
#define maj(x,y,z)      (((x) & (y)) ^ ((x) & (z)) ^ ((y) & (z)))
#define SHA1_MASK   (ZIP_SHA1_BLOCK_SIZE - 1)

#ifdef ZIP_ARCHIVE_LITTLE_ENDIAN
	#if defined(__GNUC__) && !defined(__MINGW32__)
		#include <byteswap.h>	
	#endif
	#if defined(bswap_32)
		#define swap_b32(x) bswap_32(x)
	#else
		#define swap_b32(x) ((rotl32((x), 8) & 0x00ff00ff) | (rotl32((x), 24) & 0xff00ff00))
	#endif
static UINT m_pMask[4] = {   0x00000000, 0x000000ff, 0x0000ffff, 0x00ffffff };
static UINT m_pBits[4] = {   0x00000080, 0x00008000, 0x00800000, 0x80000000 };
#else
	#define swap_b32(x) (x)
	static UINT m_pMask[4] = {   0x00000000, 0xff000000, 0xffff0000, 0xffffff00 };
	static UINT m_pBits[4] = {   0x80000000, 0x00800000, 0x00008000, 0x00000080 };
#endif

CSha1::CSha1()
{
	memset(m_count, 0, 2 * sizeof(UINT));
	memset(m_hash, 0, 5 * sizeof(UINT));
	memset(m_wbuf, 0, 16 * sizeof(UINT));
}

void CSha1::Compile()
{
	UINT w[80], i, a, b, c, d, e, t;

    /* note that words are compiled from the buffer into 32-bit */
    /* words in big-endian order so an order reversal is needed */
    /* here on little endian machines                           */
    for(i = 0; i < ZIP_SHA1_BLOCK_SIZE / 4; ++i)
		w[i] = swap_b32(m_wbuf[i]);

    for(i = ZIP_SHA1_BLOCK_SIZE / 4; i < 80; ++i)
        w[i] = rotl32(w[i - 3] ^ w[i - 8] ^ w[i - 14] ^ w[i - 16], 1);

    a = m_hash[0];
    b = m_hash[1];
    c = m_hash[2];
    d = m_hash[3];
    e = m_hash[4];

    for(i = 0; i < 20; ++i)
	{
        rnd(ch, 0x5a827999);
	}

    for(i = 20; i < 40; ++i)
	{
        rnd(parity, 0x6ed9eba1);
	}

    for(i = 40; i < 60; ++i)
	{
        rnd(maj, 0x8f1bbcdc);
	}

    for(i = 60; i < 80; ++i)
	{
        rnd(parity, 0xca62c1d6);
	}

    m_hash[0] += a;
    m_hash[1] += b;
    m_hash[2] += c;
    m_hash[3] += d;
    m_hash[4] += e;
}

void CSha1::Begin()
{
	m_count[0] = m_count[1] = 0;
    m_hash[0] = 0x67452301;
    m_hash[1] = 0xefcdab89;
    m_hash[2] = 0x98badcfe;
    m_hash[3] = 0x10325476;
    m_hash[4] = 0xc3d2e1f0;
}

void CSha1::Hash(const char* pData, UINT uSize)
{
	UINT pos = m_count[0] & SHA1_MASK;
	UINT space = ZIP_SHA1_BLOCK_SIZE - pos;

    if((m_count[0] += uSize) < uSize)
        ++(m_count[1]);

    while(uSize >= space)     /* tranfer whole blocks if possible  */
    {
        memcpy(((char*)m_wbuf) + pos, pData, space);
        pData += space;
		uSize -= space;
		space = ZIP_SHA1_BLOCK_SIZE;
		pos = 0;
        Compile();
    }

    /*lint -e{803} conceivable data overrun */
    /* there are two cases: the above while loop entered or not */
    /* entered. If not entered, 'space = ZIP_SHA1_BLOCK_SIZE - pos' */
    /* and 'len < space' so that 'len + pos < ZIP_SHA1_BLOCK_SIZE'. */
    /* If entered, 'pos = 0', 'space = ZIP_SHA1_BLOCK_SIZE' and     */
    /* 'len < space' so that 'pos + len < ZIP_SHA1_BLOCK_SIZE'. In  */
    /* both cases, therefore, the memory copy is in the buffer  */

    memcpy(((char*)m_wbuf) + pos, pData, uSize);
}



void CSha1::End(char* pData)
{
	UINT i = m_count[0] & SHA1_MASK;

    /* mask out the rest of any partial 32-bit word and then set    */
    /* the next byte to 0x80. On big-endian machines any bytes in   */
    /* the buffer will be at the top end of 32 bit words, on little */
    /* endian machines they will be at the bottom. Hence the AND    */
    /* and OR masks above are reversed for little endian systems    */
    /* Note that we can always add the first padding byte at this   */
    /* point because the buffer always has at least one empty slot  */
	m_wbuf[i >> 2] = (m_wbuf[i >> 2] & m_pMask[i & 3]) | m_pBits[i & 3];

    /* we need 9 or more empty positions, one for the padding byte  */
    /* (above) and eight for the length count.  If there is not     */
    /* enough space pad and empty the buffer                        */
    if(i > ZIP_SHA1_BLOCK_SIZE - 9)
    {
        if (i < 60)
			m_wbuf[15] = 0;
		Compile();        
        i = 0;
    }
    else    /* compute a word index for the empty buffer positions  */
        i = (i >> 2) + 1;

    while(i < 14) /* and zero pad all but last two positions        */
        m_wbuf[i++] = 0;

    /* assemble the eight byte counter in in big-endian format      */
    m_wbuf[14] = swap_b32((m_count[1] << 3) | (m_count[0] >> 29));
    m_wbuf[15] = swap_b32(m_count[0] << 3);

    Compile();

    /* extract the hash value as bytes in case the hash buffer is   */
    /* misaligned for 32-bit words                                  */
    /*lint -e{504} unusual shift operation (unusually formed right argument) */
    for(i = 0; i < ZIP_SHA1_DIGEST_SIZE; ++i)
        pData[i] = (char)((m_hash[i >> 2] >> (8 * (~i & 3)))&0xFF);
}

#endif
